----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    15:43:10 10/30/2014 
-- design name: 
-- module name:    adder_8bs2 - behavioral 
-- project name: 
-- target devices: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity adder_8bs2 is
	port(
		clk				: in std_logic;
		rst				: in std_logic;
		en				: in std_logic;
		op_a			: in std_logic_vector(7 downto 0);
		op_b			: in std_logic_vector(7 downto 0);
		carr_in		: in std_logic;
		result		   : out std_logic_vector(7 downto 0);
		carr_out	: out std_logic
		);
end adder_8bs2;

architecture behavioral of adder_8bs2 is

	-- component declaration
	component adder is
		generic(
			g_width		: natural := 8
			);
		port(
			op_a			: in std_logic_vector(g_width-1 downto 0);
			op_b			: in std_logic_vector(g_width-1 downto 0);
			carr_in		: in std_logic;
			result		   : out std_logic_vector(g_width-1 downto 0);
			carr_out	: out std_logic
			);
	end component adder;

	-- signal declaration
	signal result_low			: std_logic_vector(3 downto 0);
	signal carry_out_low						: std_logic;

	signal op_a_ret, op_b_ret : std_logic_vector(3 downto 0);
	signal result_ret			: std_logic_vector(3 downto 0);
	signal carry_ret						: std_logic;

begin

	i_adder_low : adder
		generic map(
			g_width	=> 4
			)
		port map(
			op_a			=> op_a(3 downto 0),
			op_b			=> op_b(3 downto 0),
			carr_in		=> carr_in,
			result		   => result_low,
			carr_out	=> carry_out_low
			);
			
	i_adder_high : adder
		generic map(
			g_width	=> 4
			)
		port map(
			op_a			=> op_a_ret,
			op_b			=> op_b_ret,
			carr_in		=> carry_ret,
			result		   => result(7 downto 4),
			carr_out	=> carr_out
			);
			
	result(3 downto 0) <= result_ret;

	p_seg_delay : process(clk, rst)
	begin
		if rst='1' then
			op_a_ret <= (others => '0');
			op_b_ret <= (others => '0');
			result_ret <= (others => '0');
			carry_ret <= '0';
		elsif rising_edge(clk) and en='1' then
			op_a_ret <= op_a(7 downto 4);
			op_b_ret <= op_b(7 downto 4);
			result_ret <= result_low;
			carry_ret <=carry_out_low;
		end if;
	end process p_seg_delay;

end behavioral;

