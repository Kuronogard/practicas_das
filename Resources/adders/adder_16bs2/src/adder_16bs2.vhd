----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    15:06:26 10/24/2014 
-- design name: 
-- module name:    adder_16bs2 - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity adder_16bs2 is
	port(
		clk			: in std_logic;
		rst			: in std_logic;
		en				: in std_logic;
		op_a			: in std_logic_vector(15 downto 0);
		op_b			: in std_logic_vector(15 downto 0);
		carr_in		: in std_logic;
		result		: out std_logic_vector(15 downto 0);
		carr_out	   : out std_logic
		);
end adder_16bs2;

architecture behavioral of adder_16bs2 is

	-- component declaration
	component adder_8b is
		port(
			op_a			: in std_logic_vector(7 downto 0);
			op_b			: in std_logic_vector(7 downto 0);
			carr_in		: in std_logic;
			result		: out std_logic_vector(7 downto 0);
			carr_out	   : out std_logic
			);
	end component adder_8b;
	
	-- signal declaration
	signal carr, carr_ret    : std_logic;
	signal op_a_ret          : std_logic_vector(7 downto 0);
	signal op_b_ret          : std_logic_vector(7 downto 0);
	signal result_ret        : std_logic_vector(7 downto 0);

begin

	add_low : adder_8b
		port map(
			op_a		 => op_a(7 downto 0),
			op_b		 => op_b(7 downto 0),
			carr_in	 => carr_in,
			result	 => result_ret,
			carr_out	 => carr
			);

	add_high : adder_8b
		port map(
			op_a		 => op_a_ret,
			op_b		 => op_b_ret,
			carr_in	 => carr_ret,
			result	 => result(15 downto 8),
			carr_out	 => carr_out
			);
	

	p_carr_ret : process(clk, rst)
	begin
		if rst='1' then
			carr_ret <= '0';
			op_a_ret <= (others => '0');
			op_b_ret <= (others => '0');
			result(7 downto 0) <= (others => '0');
		elsif rising_edge(clk) then
			if en='1' then
				carr_ret <= carr;
				op_a_ret <= op_a(15 downto 8);
				op_b_ret <= op_b(15 downto 8);
				result(7 downto 0) <= result_ret;
			end if;
		end if;
	end process p_carr_ret;
	

end behavioral;

