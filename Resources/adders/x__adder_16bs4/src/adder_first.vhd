----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    20:57:15 11/08/2014 
-- design name: 
-- module name:    add_4bd1o2i - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity adder_first is
	generic(
		g_delay_out : natural := 1;
		g_width			: natural := 4
	);
	port(
		clk				: in std_logic;
		rst				: in std_logic;
		en				: in std_logic;
		carry_in 	: in std_logic;
		op_a			: in std_logic_vector(g_width-1 downto 0);
		op_b			: in std_logic_vector(g_width-1 downto 0);
		carry_out	: out std_logic;
		result			: out std_logic_vector(g_width-1 downto 0)
	);
end adder_first;

architecture behavioral of adder_first is

	-- component declarations
	component adder is
		generic(
			g_width		: natural := 8
			);
		port(
			op_a			: in std_logic_vector(g_width-1 downto 0);
			op_b			: in std_logic_vector(g_width-1 downto 0);
			carr_in		: in std_logic;
			result		   : out std_logic_vector(g_width-1 downto 0);
			carr_out	: out std_logic
			);
	end component adder;

	-- type definitions
	type output_delay_type is array(0 to g_delay_out-1) of std_logic_vector(g_width-1 downto 0);

	-- signal declarations
	signal add_result : std_logic_vector(g_width-1 downto 0);
	signal output : output_delay_type;
	signal adder_carry_o : std_logic;
	
begin

	i_add : adder
		generic map(
			g_width		=> g_width
			)
		port map(
			op_a			=> op_a,
			op_b			=> op_b,
			carr_in		=> carry_in,
			result		   => add_result,
			carr_out	=> adder_carry_o
			);

	result <= output(g_delay_out-1);

	p_delay_out : process(clk, rst)
	begin
		if rst='1' then
			for i in 0 to g_delay_out-1 loop
				output(i) <= (others => '0');
			end loop;
		elsif rising_edge(clk) and en='1' then
			output(0) <= add_result;
			if g_delay_out > 1 then
				for i in 1 to g_delay_out-1 loop
					output(i) <= output(i-1);
				end loop;
			end if;
		end if;
	end process p_delay_out;

	p_carry_out : process(clk, rst)
	begin
		if rst='1' then
			carry_out <= '0';
		elsif rising_edge(clk) and en='1' then
			carry_out <= adder_carry_o;
		end if;
	end process p_carry_out;

end behavioral;

