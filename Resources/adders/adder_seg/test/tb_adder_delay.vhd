----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    14:26:58 10/25/2014 
-- design name: 
-- module name:    tb_adder_16bs2 - testbench 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_adder_delay is
end tb_adder_delay;

architecture testbench of tb_adder_delay is
	
	component adder_first is
		generic(
			g_delay_out : natural := 1;
			g_width			: natural := 4
		);
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			carry_in 	: in std_logic;
			op_a			: in std_logic_vector(g_width-1 downto 0);
			op_b			: in std_logic_vector(g_width-1 downto 0);
			carry_out	: out std_logic;
			result			: out std_logic_vector(g_width-1 downto 0)
		);
	end component adder_first;


	-- constant declaratinos
	constant clk_period : time := 100 us;
	
	-- signal declarations
	signal clk, rst, en : std_logic;
	signal op_a, op_b, result : std_logic_vector(3 downto 0);
	signal carr_in, carr_out : std_logic;

begin

	i_dut : adder_first
		generic map (
			g_delay_out => 3,
			g_width			=> 4
		)
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> en,
			carry_in 	=> carr_in,
			op_a			=> op_a,
			op_b			=> op_b,
			carry_out	=> carr_out,
			result			=> result
		);


	p_rst : process
	begin
		rst <= '1';
		en  <= '0';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		en  <= '1';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	
	p_stim : process
	begin
		wait until rst='0';
		op_a <= std_logic_vector(to_unsigned(3, 4));
		op_b <= std_logic_vector(to_unsigned(8, 4));
		carr_in <= '1';
		wait for clk_period;
		op_a <= std_logic_vector(to_unsigned(7, 4));
		op_b <= std_logic_vector(to_unsigned(2, 4));
		carr_in <= '0';	
	end process p_stim;

end testbench;

