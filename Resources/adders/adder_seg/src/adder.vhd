----------------------------------------------------------------------------------
-- author:		Daniel Pinto Rivero
-- create date:		09:42:44 10/24/2014 
-- module name:	adder_8b - behavioral 
-- project name: 
-- target devices: 	spartan-3
-- description:   8 bit adder
-- 	
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity adder is
	generic(
		g_width		: natural := 8
		);
	port(
		op_a			: in std_logic_vector(g_width-1 downto 0);
		op_b			: in std_logic_vector(g_width-1 downto 0);
		carr_in		: in std_logic;
		result		   : out std_logic_vector(g_width-1 downto 0);
		carr_out	: out std_logic
		);
end adder;

architecture behavioral of adder is
begin


	p_result : process(op_a, op_b, carr_in)
		variable carry : std_logic_vector(g_width downto 0);
	begin
		carry(0) := carr_in;
		for i in 0 to g_width-1 loop
			carry(i+1) := ( op_a(i) and op_b(i) ) or ( op_a(i) and carry(i) ) or ( op_b(i) and carry(i) );
			result(i) <= op_a(i) xor op_b(i) xor carry(i);
		end loop;
		carr_out <= carry(g_width);
	end process p_result;

end behavioral;

