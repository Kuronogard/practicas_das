----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    12:46:26 11/02/2014 
-- design name: 
-- module name:    adder_16bs4 - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity adder_16bs4 is
	port(
		clk				: in std_logic;
		rst				: in std_logic;
		en				: in std_logic;
		op_a			: in std_logic_vector(15 downto 0);
		op_b			: in std_logic_vector(15 downto 0);
		carr_in		: in std_logic;
		result		   : out std_logic_vector(15 downto 0);
		carr_out	: out std_logic
		);
end adder_16bs4;

architecture behavioral of adder_16bs4 is

	component adder_delay is
		generic(
			g_delay_in : natural := 1;
			g_delay_out : natural := 1;
			g_width			: natural := 4
		);
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			carry_in 	: in std_logic;
			op_a			: in std_logic_vector(g_width-1 downto 0);
			op_b			: in std_logic_vector(g_width-1 downto 0);
			carry_out	: out std_logic;
			result			: out std_logic_vector(g_width-1 downto 0)
		);
	end component adder_delay;

	-- component declaration
--	component adder_first is
--		generic(
--			g_delay_out : natural := 1;
--			g_width			: natural := 4
--		);
--		port(
--			clk				: in std_logic;
--			rst				: in std_logic;
--			en				: in std_logic;
--			carry_in 	: in std_logic;
--			op_a			: in std_logic_vector(g_width-1 downto 0);
--			op_b			: in std_logic_vector(g_width-1 downto 0);
--			carry_out	: out std_logic;
--			result			: out std_logic_vector(g_width-1 downto 0)
--		);
--	end component adder_first;

	component adder_middle is
		generic(
			g_delay_in : natural := 1;
			g_delay_out : natural := 1;
			g_width			: natural := 4
		);
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			carry_in 	: in std_logic;
			op_a			: in std_logic_vector(g_width-1 downto 0);
			op_b			: in std_logic_vector(g_width-1 downto 0);
			carry_out	: out std_logic;
			result			: out std_logic_vector(g_width-1 downto 0)
		);
	end component adder_middle;
	
	component adder_last is
		generic(
			g_delay_in : natural := 1;
			g_width			: natural := 4
		);
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			carry_in 	: in std_logic;
			op_a			: in std_logic_vector(g_width-1 downto 0);
			op_b			: in std_logic_vector(g_width-1 downto 0);
			carry_out	: out std_logic;
			result			: out std_logic_vector(g_width-1 downto 0)
		);
	end component adder_last;

	-- constant declarations
	constant c_seg : natural := 2;
	constant g_width : natural := 16;

	-- type definitions
	type carry_type is array(0 to 4) of std_logic;
	type output_type is array(0 to 3) of std_logic_vector(3 downto 0);

	-- signal declaration
	signal carry : carry_type;
	signal output : output_type;

begin

	carry(0) <= carr_in;
	carr_out <= carry(4);

	adders : for i in 0 to 3 generate
		i_adder : adder_delay
			generic map(
				g_delay_in 		=> i,
				g_delay_out 	=> 3-i,
				g_width				=> 4
			)
			port map(
				clk				=> clk,
				rst				=> rst,
				en				=> en,
				carry_in 	=> carry(i),
				op_a			=> op_a(4*(i+1)-1 downto 4*i),
				op_b			=> op_b(4*(i+1)-1 downto 4*i),
				carry_out	=> carry(i+1),
				result			=> output(i)
			);
	end generate adders;


----	i_adder_first : adder_first
----		generic map(
----			g_delay_out 	=> 3,
----			g_width				=> 2
----		)
----		port map(
----			clk				=> clk,
----			rst				=> rst,
----			en				=> en,
----			carry_in 	=> carry(0),
----			op_a			=> op_a(1 downto 0),
----			op_b			=> op_b(1 downto 0),
----			carry_out	=> carry(1),
----			result			=> output(0)
----		);
--
--		i_adder : adder_last
--			generic map(
--				g_delay_in 		=> 7,
--				g_width				=> 2
--			)
--			port map(
--				clk				=> clk,
--				rst				=> rst,
--				en				=> en,
--				carry_in 	=> carry(7),
--				op_a			=> op_a(15 downto 14),
--				op_b			=> op_b(15 downto 14),
--				carry_out	=> carry(8),
--				result			=> output(7)
--			);
--
--	adders : for i in 0 to 6 generate
--		i_adder : adder_middle
--			generic map(
--				g_delay_in 		=> i+1,
--				g_delay_out 	=> 7-i,
--				g_width				=> 2
--			)
--			port map(
--				clk				=> clk,
--				rst				=> rst,
--				en				=> en,
--				carry_in 	=> carry(i),
--				op_a			=> op_a(2*(i+1)-1 downto 2*i),
--				op_b			=> op_b(2*(i+1)-1 downto 2*i),
--				carry_out	=> carry(i+1),
--				result			=> output(i)
--			);
--	end generate adders;
	
	p_output : process( output )
	begin
		for i in 0 to 3 loop
			result(4*(i+1)-1 downto 4*i) <= output(i);
		end loop;
	end process p_output;


end behavioral;

