----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:48:03 10/12/2014 
-- Design Name: 
-- Module Name:    pr1_a - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

entity reg is
   generic( 
      g_width : natural := 8
   );
   port( 
      clk : in     std_logic;
      d   : in     std_logic_vector (g_width - 1 downto 0);
      en  : in     std_logic;
      rst : in     std_logic;
      q   : out    std_logic_vector (g_width - 1 downto 0)
   );
end entity reg ;



architecture behavioral of reg is

begin
   p_reg: process (clk, rst)
   begin
		if rst='1' then
			q <= (others => '0');
		elsif rising_edge(clk) then
			if en ='1' then
				q <= d;
         end if;
       end if;
   end process p_reg;   

end architecture behavioral;
