----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    12:23:13 10/25/2014 
-- design name: 
-- module name:    comparator - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity comparator is
	generic(
		g_width : natural := 8
	);
	port(
		op_a : in std_logic_vector(g_width-1 downto 0);
		op_b : in std_logic_vector(g_width-1 downto 0);
		result: out std_logic
	);
end comparator;

architecture behavioral of comparator is
	signal intermediate_result : std_logic_vector(g_width-1 downto 0);

begin

	p_and : process(op_a, op_b)
	begin
		for i in 0 to g_width-1 loop
			intermediate_result(i) <= not( op_a(i) xor op_b(i) );
		end loop;
	end process p_and;

	p_result : process(intermediate_result)
		variable aux_result : std_logic;
	begin
		aux_result := '1';
		for i in 0 to g_width-1 loop
			aux_result := aux_result and intermediate_result(i);
		end loop;
		result <= aux_result;
	end process p_result;

end behavioral;

