----------------------------------------------------------------------------------
-- company: 
-- engineer:  daniel pinto rivero
-- 
-- create date:    10:04:28 10/24/2014 
-- design name: 
-- module name:    tb_adder_8b - testbench 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_counter_a16bs2 is
end tb_counter_a16bs2;

architecture testbench of tb_counter_a16bs2 is
	-- component declaration
	component counter_a16bs2 is
		port(
			clk       : in std_logic;
			rst       : in std_logic;
			en        : in std_logic;
			restart	: in std_logic;
			count		: out std_logic_vector(15 downto 0)
		);
	end component counter_a16bs2;

	
	-- constant declaratinos
	constant clk_period : time := 10 us;
	
	-- signal declarations
	signal clk, rst, en : std_logic;
	signal restart : std_logic;
	signal count : std_logic_vector(15 downto 0);
	
begin

	i_dut : counter_a16bs2
		port map(
			clk       => clk,
			rst       => rst,
			en        => en,
			restart	=> restart,
			count		=> count
		);
	
	p_rst : process
	begin
		rst <= '1';
		en  <= '0';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		en  <= '1';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	
	p_stim : process
	begin
		restart <= '0';	
		wait until count = "0000000100101111";
		wait until falling_edge(clk);
		restart <= '1';
		wait for clk_period*2;
	end process p_stim;

end testbench;

