----------------------------------------------------------------------------------
-- company: 
-- engineer:  daniel pinto rivero
-- 
-- create date:    10:04:28 10/24/2014 
-- design name: 
-- module name:    tb_adder_8b - testbench 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_counter_ctc_16bs2 is
end tb_counter_ctc_16bs2;

architecture testbench of tb_counter_ctc_16bs2 is
	-- component declaration
	component counter_ctc_16bs2 is
		port(
			clk       : in std_logic;
			rst       : in std_logic;
			en        : in std_logic;
			max_count : in std_logic_vector(15 downto 0);
			reached   : out std_logic
		);
	end component counter_ctc_16bs2;

	
	-- constant declaratinos
	constant clk_period : time := 10 us;
	
	-- signal declarations
	signal clk, rst, en : std_logic;
	signal reached : std_logic;
	
begin
	i_dut : counter_ctc_16bs2
		port map(
			clk       => clk,
			rst       => rst,
			en        => en,
			max_count => std_logic_vector(to_unsigned(100, 16)),
			reached   => reached
		);
	
	p_rst : process
	begin
		rst <= '1';
		en  <= '0';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		en  <= '1';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	
	p_stim : process
	begin
		wait for clk_period*3;
		wait until falling_edge(clk);

		wait;
	end process p_stim;

end testbench;

