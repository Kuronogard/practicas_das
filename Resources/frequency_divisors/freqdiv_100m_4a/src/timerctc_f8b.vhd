----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    18:18:24 10/24/2014 
-- design name: 
-- module name:    counter - behavioral 
-- project name: 
-- target devices: 
--
-- while en='1', counts to "max_count" and then reset to 0 to
-- keep counting
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity timerctc_f8b is
	port(
		clk       : in std_logic;
		rst       : in std_logic;
		en        : in std_logic;
		max_count : in std_logic_vector(7 downto 0);
		reached   : out std_logic
	);
end timerctc_f8b;

architecture behavioral of timerctc_f8b is

	-- component declaration
	component timer_f is
		generic(
			g_width : natural := 8
		);
		port(
			clk            : in std_logic;
			rst             : in std_logic;
			en             : in std_logic;
			restart       : in std_logic;
			count			 : out std_logic_vector(g_width-1 downto 0)
		);
	end component timer_f;	

	component comparator is
		generic(
			g_width : natural := 8
		);
		port(
			op_a : in std_logic_vector(g_width-1 downto 0);
			op_b : in std_logic_vector(g_width-1 downto 0);
			result: out std_logic
		);
	end component comparator;
	
	--signal declaration
	signal current_count  : std_logic_vector(7 downto 0);
	signal comp : std_logic;

begin

	i_timer : timer_f
		generic map(
			g_width => 8
		)
		port map(
			clk        => clk,
			rst        => rst,
			en        => en,
			restart  => comp,
		   count    => current_count
		);

	i_comparator : comparator
		generic map(
			g_width => 8
		)
		port map(
			op_a 		=> current_count,
			op_b 		=> max_count,
			result		=> comp
		);	
	
	reached <= comp;
	
end behavioral;

