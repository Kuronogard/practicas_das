----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    18:16:04 10/25/2014 
-- design name: 
-- module name:    jkflipflop - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity jkflipflop is
   port( 
         clk		: in std_logic;
         rst		: in std_logic;
         en		: in std_logic;
			j			: in  std_logic;
			k			: in  std_logic;
         q			: out std_logic
			);
end jkflipflop;
 
architecture behavioral of jkflipflop is
   signal temp: std_logic;
begin
   
	p_result : process (clk, rst) 
   begin
		if rst='1' then
			temp <= '0';
      elsif rising_edge(clk) then                 
			if en ='1' then
            if (j='0' and k='0') then
               temp <= temp;
            elsif (j='0' and k='1') then
               temp <= '0';
				elsif (j='1' and k='0') then
					temp <= '1';
				elsif (j='1' and k='1') then
					temp <= not temp;
            end if;
         end if;
      end if;
   end process;
   
	q <= temp;
	
end behavioral;

