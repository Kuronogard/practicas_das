----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    21:32:01 10/27/2014 
-- design name: 
-- module name:    freq_div_100m_4a - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity freqdiv_100m_4a is
	port(
		clk				: in std_logic;
		rst				: in std_logic;
		en				: in std_logic;
		clk_100k	: out std_logic;
		clk_100h	: out std_logic;
		clk_2h		: out std_logic;
		clk_1h		: out std_logic
	);
end freqdiv_100m_4a;

architecture behavioral of freqdiv_100m_4a is

	-- component declaration
	component timerctc_a16bs2 is
		port(
			clk       : in std_logic;
			rst       : in std_logic;
			en        : in std_logic;
			max_count : in std_logic_vector(15 downto 0);
			reached   : out std_logic
		);
	end component timerctc_a16bs2;
	
	component timerctc_f8b is
		port(
			clk       : in std_logic;
			rst       : in std_logic;
			en        : in std_logic;
			max_count : in std_logic_vector(7 downto 0);
			reached   : out std_logic
		);
	end component timerctc_f8b;
	
	component jkflipflop is
		port( 
				clk		: in std_logic;
				rst		: in std_logic;
				en		: in std_logic;
				j			: in  std_logic;
				k			: in  std_logic;
				q			: out std_logic
				);
	end component jkflipflop;

	component rise_edge_detector is
		port(
			clk				: in std_logic;
			rst	   		: in std_logic;
			en				: in std_logic;
			signal_in	: in std_logic;
			edge			: out std_logic
		);
	end component rise_edge_detector;

	-- signal declaration
		signal max_reached, freqs : std_logic_vector(3 downto 0);

begin
	-- divisor de frecuencia con entrada a 100MHz y salidas 100KHz, 100Hz, 2Hz, 1Hz (clk/1000, 100KHz/1000, 100Hz/50, 2Hz/2)
	-- voy a usar 2 contadores de 16bits (segmentados en 2 ciclos) y un contador de 8 bits

	clk_100k <= freqs(0);
	clk_100h <= freqs(1);
	clk_2h     <= freqs(2);
	clk_1h     <= freqs(3);
	
	edge_detection : for i in 0 to 3 generate
		i_rise_detector : rise_edge_detector
			port map(
				clk				=> clk,
				rst	   		=> rst,
				en				=> '1',
				signal_in	=> max_reached(i),
				edge			=> freqs(i)
			);
	
	end generate edge_detection;
	
	i_timer_100k : timerctc_a16bs2
		port map(
			clk        		=> clk,
			rst       		=> rst,
			en        		=> en,
			max_count =>"0000001111100111",   -- 1000-1		512+256=768+128=896+64=960+32=992+4+2+1=999
			reached   	=> max_reached(0)
		);
	
	i_timer_100h : timerctc_a16bs2
		port map(
			clk        		=> clk,
			rst       		=> rst,
			en        		=> freqs(0),
			max_count => "0000001111100111",   -- 1000-1		512+256=768+128=896+64=960+32=992+4+2+1=999
			reached   	=> max_reached(1)
		);
		
	i_timer_2h : timerctc_f8b
		port map(
			clk        		=> clk,
			rst       		=> rst,
			en        		=> freqs(1),
			max_count =>"00110001",   -- 50-1
			reached   	=> max_reached(2)
		);
		
	i_timer_1h : jkflipflop
		port map( 
				clk		=> clk,
				rst		=> rst,
				en		=> freqs(2),
				j			=> '1',
				k			=> '1',
				q			=> max_reached(3)
				);

end behavioral;

