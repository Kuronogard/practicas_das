----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    00:57:03 10/26/2014 
-- design name: 
-- module name:    tb_freq_div_100mho1h - testbench 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity tb_freqdiv_50mho1h is
end tb_freqdiv_50mho1h;

architecture testbench of tb_freqdiv_50mho1h is

	-- component declaration
	component freqdiv_50m1h is
		port(
			clk		   : in std_logic;
			rst		   : in std_logic;
			en		   : in std_logic;
			clk_1h	: out std_logic
			);
	end component freqdiv_50m1h;

	-- constant declaratinos
	constant clk_period : time := 10 ns;
	
	-- signal declarations
	signal clk, rst, en : std_logic;
	signal clk_div : std_logic;
	
begin

	i_dut : freqdiv_50m1h
		port map(
			clk		   => clk,
			rst		   => rst,
			en		   => en,
			clk_1h	=> clk_div
			);

	p_rst : process
	begin
		rst <= '1';
		en  <= '0';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		en  <= '1';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	
	p_stim : process
	begin
		wait for clk_period*3;
		wait until falling_edge(clk);

		wait;
	end process p_stim;


end testbench;

