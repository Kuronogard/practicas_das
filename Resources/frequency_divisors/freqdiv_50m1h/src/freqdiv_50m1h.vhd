----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    21:32:01 10/27/2014 
-- design name: 
-- module name:    freq_div_100m_4a - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity freqdiv_50m1h is
	port(
		clk				: in std_logic;
		rst				: in std_logic;
		en				: in std_logic;
		clk_1h		: out std_logic
	);
end freqdiv_50m1h;

architecture behavioral of freqdiv_50m1h is

	-- component declaration
	component timerctc_16bs2 is
		port(
			clk       : in std_logic;
			rst       : in std_logic;
			en        : in std_logic;
			max_count : in std_logic_vector(15 downto 0);
			reached   : out std_logic
		);
	end component timerctc_16bs2;

	component rise_edge_detector is
		port(
			clk				: in std_logic;
			rst	   		: in std_logic;
			en				: in std_logic;
			signal_in	: in std_logic;
			edge			: out std_logic
		);
	end component rise_edge_detector;

	-- signal declaration
		signal max_reached, freqs : std_logic_vector(1 downto 0);

begin

	clk_1h     <= freqs(1);
	
	edge_detection : for i in 0 to 1 generate
		i_rise_detector : rise_edge_detector
			port map(
				clk				=> clk,
				rst	   		=> rst,
				en				=> '1',
				signal_in	=> max_reached(i),
				edge			=> freqs(i)
			);
	
	end generate edge_detection;
	
	i_timer_10k : timerctc_16bs2
		port map(
			clk        		=> clk,
			rst       		=> rst,
			en        		=> en,
			max_count =>"0110000110100111",   -- 25000-1
			reached   	=> max_reached(0)
		);
	
	i_timer_1h : timerctc_16bs2
		port map(
			clk        		=> clk,
			rst       		=> rst,
			en        		=> freqs(0),
			max_count => "0000011111001111",   -- 2000-1
			reached   	=> max_reached(1)
		);

end behavioral;

