----------------------------------------------------------------------------------
-- company: 
-- engineer:  daniel pinto rivero
-- 
-- create date:    10:04:28 10/24/2014 
-- design name: 
-- module name:    tb_adder_8b - testbench 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_ps2_interface_driver_send is
end tb_ps2_interface_driver_send;

architecture testbench of tb_ps2_interface_driver_send is

	component ps2interface is
		port(
			ps2_clk  : inout std_logic;
			ps2_data : inout std_logic;
			clk      : in std_logic;
			rst      : in std_logic;
			tx_data  : in std_logic_vector(7 downto 0);
			write    : in std_logic;
			rx_data  : out std_logic_vector(7 downto 0);
			read     : out std_logic;
			busy     : out std_logic;
			err      : out std_logic
	);
	end component ps2interface;

	
	-- constant declaratinos
	constant clk_period : time := 10 us;
	constant s_clk_period	: time := clk_period*20;
	
	-- signal declarations
	signal clk, rst : std_logic;
	signal s_clk, s_data : std_logic;
	signal start_clk : std_logic;
	signal success, busy, write_start, error: std_logic;
	signal received_byte, write_byte : std_logic_vector(7 downto 0);
	
	
	-- variable reclarations
	signal start_clock : std_logic;
	
begin
			
	i_dut : ps2interface
		port map(
			clk					=> clk,
			rst					=> rst,
			ps2_clk			=> s_clk,
			ps2_data		=> s_data,
			write				=> write_start,
			tx_data			=> write_byte,
			read				=> success,
			busy				=> busy,
			rx_data			=> received_byte,
			err					=> error
		);
	
	
	p_rst : process
	begin
		rst <= '1';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	p_s_clk : process
	begin
		s_clk <= 'Z';
		wait until rst='0';
		wait for clk_period*5;
		wait until start_clk='1';
		wait until falling_edge(clk);
		for i in 0 to 10 loop
			wait for s_clk_period/2;
			s_clk <= '0';
			wait for s_clk_period/2;
			s_clk <= '1';
		end loop;
		wait;
	end process p_s_clk;
	
	p_stim : process
		variable data : std_logic_vector(10 downto 0) := "11001110010";   -- 01111001 parity->1
	begin
		s_data <= 'Z';
		start_clk <= '0';
		write_byte <= data(8 downto 1);
		write_start <= '1';
		wait for clk_period*6050;
		write_start <= '0';
		start_clk <= '1';
		
--		for i in 0 to 10 loop
--			s_data <= data(i);
--			wait until rising_edge(s_clk);
--		end loop;
		wait;
	end process p_stim;

end testbench;

