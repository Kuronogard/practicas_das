----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    20:46:08 11/13/2014 
-- design name: 
-- module name:    serial_bit_detector - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity serial_bit_detector is
	generic(
		g_filter_width : natural := 12
	);
	port(
		clk					: in std_logic;
		rst					: in std_logic;
		s_clk				: in std_logic;
		s_data			: in std_logic;
		s_clk_out		: out std_logic;
		s_data_out	: out std_logic
	);
end serial_bit_detector;

architecture behavioral of serial_bit_detector is

	-- type definitions
	--type shift_filter_array is array(0 to g_filter_width-1) of std_logic;
	
	-- signal declarations
	--signal shift_clk, shift_data : shift_filter_array;

begin

	s_clk_out <= s_clk;
	s_data_out <= s_data;


end behavioral;

