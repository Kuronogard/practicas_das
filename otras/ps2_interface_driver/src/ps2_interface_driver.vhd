----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    21:41:56 11/13/2014 
-- design name: 
-- module name:    ps2_interface_driver - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity ps2_interface_driver is
	port(
		clk			: in std_logic;
		rst			: in std_logic;
		s_clk			: inout std_logic;
		s_data		: inout std_logic;
		write_start	: in std_logic;
		write_byte	: in std_logic_vector(7 downto 0);
		success		: out std_logic;
		busy			: out std_logic;
		error			: out std_logic;
		read_byte	: out std_logic_vector(7 downto 0)
	);
end ps2_interface_driver;

architecture behavioral of ps2_interface_driver is

	---------------------------------------------------------------------------------------
	-- component declarations
	---------------------------------------------------------------------------------------
	component serial_bit_detector is
		generic(
			g_filter_width : natural := 12
		);
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			s_clk				: in std_logic;
			s_data			: in std_logic;
			s_clk_out		: out std_logic;
			s_data_out	: out std_logic
		);
	end component serial_bit_detector;

	component counter_f is
		generic(
			g_width : natural := 8
		);
		port(
			clk		  : in std_logic;
			rst		  : in std_logic;
			en		  : in std_logic;
			restart  : in std_logic;
			count		: out std_logic_vector(g_width-1 downto 0)
		);
	end component counter_f;

	component counter_a16bs2 is
		port(
			clk       			: in std_logic;
			rst       			: in std_logic;
			en        		: in std_logic;
			restart			: in std_logic;
			max_count 	: in std_logic_vector(15 downto 0);
			reached   	: out std_logic
		);
	end component counter_a16bs2;

	---------------------------------------------------------------------------------------
	-- type definitions
	---------------------------------------------------------------------------------------	
	type state_type is (
			idle_st, error_st,
			rx_read_st, rx_wait_rise_st, rx_wait_fall_st, rx_success_st, rx_wait_release_st,
			tx_clk_down_st, tx_data_down_st, tx_release_clk_st, tx_wait_fall_st, tx_send_st, tx_wait_rise_st, tx_wait_ack_st, tx_wait_release_st, tx_success_st 
			);
	
	---------------------------------------------------------------------------------------
	-- signal declarations
	---------------------------------------------------------------------------------------
	-- state machine
	signal current_state, next_state : state_type;
	
	-- inout ports
	signal s_clk_h, s_data_h : std_logic;
	
	-- debounced serial lines
	signal s_clk_int, s_data_int : std_logic;
	
	-- control signals
	signal acked, ack_ld : std_logic;
	signal parity_restart, parity_calc, parity_res, parity_tx_calc, parity_tx_res : std_logic;
	signal frame_ld, frame_shift : std_logic;
	signal count_en, count_restart : std_logic;
	signal count_output : std_logic_vector(3 downto 0);
	signal delay_en,  delay_end, delay_restart : std_logic;
	signal delay_max_count : std_logic_vector(15 downto 0);
	
	-- data registers
	signal frame : std_logic_vector(7 downto 0);   -- (d0-d7)
	signal parity : std_logic;
		
begin

	-- filtro para la linea de reloj y la de datos
	i_bit_detector : serial_bit_detector
		generic map(
			g_filter_width => 12
		)
		port map(
			clk				 => clk,
			rst				 => rst,
			s_clk			 => s_clk,
			s_data		 => s_data,
			s_clk_out	 => s_clk_int,
			s_data_out => s_data_int
		);
	
	-- contador de bits (enviados o recibidos)
	i_bit_counter : counter_f
		generic map(
			g_width => 4
		)
		port map(
			clk		 	 => clk,
			rst		  	=> rst,
			en		  	=> count_en,
			restart => count_restart,
			count	=> count_output
		);
	
	-- contador para hacer los delays
	i_delay : counter_a16bs2
		port map(
			clk       			=> clk,
			rst       			=> rst,
			en        		=> delay_en,
			restart			=> delay_restart,
			max_count 	=> delay_max_count,
			reached   	=> delay_end
		);
	
	-- tristate buffers (asignar siempre a s_clk_h y s_data_h)
	s_clk <= 'Z' when s_clk_h='1' else '0';
	s_data <= 'Z' when s_data_h='1' else '0';
	
	
	-- shift register para almacenar el frame que se va a enviar o recibir
	-- bits de control: [frame_ld, frame_shift]
	p_frame : process(clk, rst)
	begin
		if rst='1' then
			frame <= (others => '0');
		elsif rising_edge(clk) then
			if frame_ld='1' then
				frame <= write_byte;
			elsif frame_shift='1' then
				frame(7) <= s_data_int;
				frame(6 downto 0) <= frame(7 downto 1);
			end if;
		end if;
	end process p_frame;
	
	p_parity : process(clk, rst)
	begin
		if rst='1' then
			parity <= '0';
		elsif rising_edge(clk) then
			if parity_restart = '1' then
				parity <= '0';
			elsif parity_res='1' then
				parity <= (not parity) and s_data_int;
			elsif parity_tx_res='1' then
				parity <= not(parity xor frame(0));
			elsif parity_calc='1' then
				parity <= parity xor s_data_int;
			elsif parity_tx_calc='1' then
				parity <= parity xor frame(0);
			end if;
		end if;
	end process p_parity;
	
	p_acked : process(clk, rst)
	begin
		if rst='1' then
			acked <= '0';
		elsif rising_edge(clk) and ack_ld='1' then
			if s_clk_int = '1' and s_data_int = '1' then
				acked <= '1';
			else
				acked <= '0';
			end if;
		end if;
	end process p_acked;
	
	
	-- =======================================================
	--		máquina de estados del driver (recepción)
	-- =======================================================
	p_current_state : process(clk, rst)
	begin
		if rst='1' then
			current_state <= idle_st;
			read_byte <= (others => '0');
		elsif rising_edge(clk) then
			current_state <= next_state;
			if current_state = rx_success_st then
				read_byte <= frame(7 downto 0);
			end if;
		end if;
	end process p_current_state;
	
	-- idle_st, rx_read_st, rx_wait_rise_st, rx_wait_fall_st, rx_parity_check, rx_success_st, rx_error_st
	-- tx_clk_down_st, tx_data_down_st, tx_release_clk_st, tx_wait_fall_st, tx_send_st, tx_wait_rise_st, tx_wait_ack_st, tx_success_st 
	-- calcula el siguiente estado
	p_next_state : process(current_state, count_output, s_clk_int, s_data_int, parity, delay_end, write_start, acked)
	begin
		next_state <= current_state; 
		case current_state is
			------------------------------------------------------------------------------
			-- idle
			------------------------------------------------------------------------------
			when idle_st =>
				if s_clk_int='0' then						-- si se baja la linea de clk, se va a recibir un dato
					next_state <= rx_wait_rise_st;
				elsif write_start = '1' then
					next_state <= tx_clk_down_st;
				end if;
			------------------------------------------------------------------------------
			-- receive states
			------------------------------------------------------------------------------
			when rx_read_st =>
				next_state <= rx_wait_rise_st;
			when rx_wait_rise_st =>
				if s_clk_int = '1' then
					next_state <= rx_wait_fall_st;
				end if;
			when rx_wait_fall_st =>
				if s_clk_int = '0' then
					if count_output = "1001" then			-- "10" ha leido los 8 bits, paridad y stop 
						next_state <= rx_wait_release_st;
					else
						next_state <= rx_read_st;
					end if;
				end if;		
			when rx_wait_release_st =>
				if s_clk_int = '1' then
					next_state <= error_st;
					if s_data_int = '1' then
						if parity = '1' then
							next_state <= rx_success_st;
						end if;
					end if;
				end if;		
			when error_st =>
				next_state <= idle_st;
			when rx_success_st =>
				next_state <= idle_st;
			------------------------------------------------------------------------------
			-- send states
			-- tx_clk_down_st, tx_data_down_st, tx_release_clk_st, tx_wait_fall_st, tx_send_st, tx_wait_rise_st, tx_wait_ack_st, tx_success_st 
			------------------------------------------------------------------------------
			when tx_clk_down_st =>
				if delay_end = '1' then
					next_state <= tx_data_down_st;
				end if;
			when tx_data_down_st =>
				if delay_end = '1' then
					next_state <= tx_release_clk_st;
				end if;
			when tx_release_clk_st =>
				if delay_end = '1' then
					if s_clk_int = '0' then
						next_state <= tx_send_st;
					end if;
				end if;
			when tx_send_st =>
				next_state <= tx_wait_rise_st;
			when tx_wait_rise_st =>
				if s_clk_int = '1' then
					if count_output = "1010" then
						next_state <= tx_wait_ack_st;
					else
						next_state <= tx_wait_fall_st;					
					end if;
				end if;
			when tx_wait_fall_st =>
				if s_clk_int = '0' then
					next_state <= tx_send_st;
				end if;
			when tx_wait_ack_st =>
				if s_clk_int = '0' then
					next_state <= tx_wait_release_st;
				end if;
			when tx_wait_release_st =>
				if s_clk_int = '1' then
					if s_data_int = '1' and parity='1' and acked='1' then
						next_state <= tx_success_st;
					else
						next_state <= error_st;
					end if;
				end if;
			when tx_success_st =>
				next_state <= idle_st;
		end case;
	end process p_next_state;

	-- calcula las salidas en función del estado actual
	p_state_machine_outputs : process(current_state, count_output, frame, parity)
	begin
			s_clk_h <= '1';
			s_data_h <= '1';
			ack_ld <= '0';
			busy <= '1';
			success <= '0';
			frame_shift <= '0';
			frame_ld <= '0';
			parity_restart <= '0';
			parity_calc <= '0';
			parity_res <= '0';
			parity_tx_calc <= '0';
			parity_tx_res <= '0';
			count_en <= '0';
			count_restart <= '0';
			delay_restart <= '0';
			delay_en <= '0';
			delay_max_count <= (others => '0');
		
		case current_state is
			------------------------------------------------------------------------------
			-- idle
			------------------------------------------------------------------------------
			when idle_st =>
				count_restart <= '1';
				count_en <= '1';
				delay_restart <= '1';
				parity_restart <= '1';
				busy <= '0';
			------------------------------------------------------------------------------
			-- receive outputs
			------------------------------------------------------------------------------
			when rx_read_st =>
				count_en <= '1';
				if count_output = "1000" then		-- ha leido 8 bits, calcular resultado de paridad
					parity_res <= '1';
				else														-- calculo parcial de paridad y guardar el valor en frame
					parity_calc <= '1';
					frame_shift <= '1';
				end if;
			when rx_wait_rise_st =>
			when rx_wait_fall_st =>
			when error_st =>
				error <= '1';
			when rx_success_st =>
				success <= '1';
			when rx_wait_release_st =>
			------------------------------------------------------------------------------
			-- send outputs
			------------------------------------------------------------------------------
			when tx_clk_down_st =>
				s_clk_h <= '0';
				frame_ld <= '1';
				delay_max_count <= "0001001110000111";		-- 5000-1
				delay_en <= '1';
			when tx_data_down_st =>
				s_clk_h <= '0';
				s_data_h <= '0';
				delay_max_count <= "0001011101101111";		-- 5000+1000-1
				delay_en <= '1';
			when tx_release_clk_st =>
				s_data_h <= '0';
				delay_max_count <= "0001011101111001";		-- 5000+1000+10-1
				delay_en <= '1';

			when tx_send_st =>
				if count_output = "1000" then
					parity_tx_res <= '1';
				elsif count_output /= "1001" and count_output /= "0000" then
					parity_tx_calc <= '1';
				end if;
				if count_output /= "0000" then
					frame_shift <= '1';
				end if;
				s_data_h <= frame(0);
				count_en <= '1';

			when tx_wait_rise_st =>
				if count_output = "1001" then
					s_data_h <= parity;
				elsif count_output /= "1010" then
					s_data_h <= frame(0);
				end if;

			when tx_wait_fall_st =>
				if count_output = "1001" then
					s_data_h <= parity;
				elsif count_output /= "1010" then
					s_data_h <= frame(0);
				end if;

			when tx_wait_ack_st =>
				ack_ld <= '1';
			when tx_wait_release_st =>
				
			when tx_success_st =>
				success <= '1';
		end case;
	end process p_state_machine_outputs;

end behavioral;

