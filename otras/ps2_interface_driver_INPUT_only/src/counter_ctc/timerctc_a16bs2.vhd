----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    18:18:24 10/24/2014 
-- design name: 
-- module name:    counter - behavioral 
-- project name: 
-- target devices: 
--
-- while en='1', counts to "max_count" and then reset to 0 to
-- keep counting
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-- cuenta hasta max_count y se queda parado ahí
-- reached vale uno mientras count=max_count

entity counter_a16bs2 is
	port(
		clk       			: in std_logic;
		rst       			: in std_logic;
		en        		: in std_logic;
		restart			: in std_logic;
		max_count 	: in std_logic_vector(15 downto 0);
		reached   	: out std_logic
	);
end counter_a16bs2;

architecture behavioral of counter_a16bs2 is

	-- component declaration
	component adder_16bs2_i is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			op_a			: in std_logic_vector(15 downto 0);
			result		: out std_logic_vector(15 downto 0)
			);
	end component adder_16bs2_i;
	
	component reg is
		generic( 
			g_width : natural := 8
			);
		port( 
			clk : in     std_logic;
			d   : in     std_logic_vector (g_width - 1 downto 0);
			en  : in     std_logic;
			rst : in     std_logic;
			q   : out    std_logic_vector (g_width - 1 downto 0)
			);
	end component reg;
	
	--signal declaration
	signal current_count, add_output, next_count : std_logic_vector(15 downto 0);

begin
	
	i_add : adder_16bs2_i
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> en,
			op_a			=> current_count,
			result			=> add_output
			);

	i_curr_count : reg
		generic map( 
			g_width => 16
			)
		port map( 
			clk 		=> clk,
			d   		=> next_count,
			en  	=> en,
			rst 		=> rst,
			q   		=> current_count
			);
	
	p_next_count : process(add_output, current_count, max_count, restart)
	begin
		if restart = '1' then														-- si debe reiniciar, la siguiente cuenta es 0
			next_count <= "0000000000000000";
		elsif current_count = max_count then							-- si ha llegado al final, se queda ahí
			next_count <= current_count;
		elsif current_count = "0000000000000000" then -- si el actual es 0, el siguiente es 1
			next_count <= ( 0 => '1', others=> '0');
		else																					-- si no, el siguiente es la salida del sumador
			next_count <= add_output;
		end if;
	end process p_next_count;

	
	p_reached : process(current_count, max_count)
	begin
		reached <= '0';
		if current_count = max_count then
			reached <= '1';
		end if;
	end process p_reached;
	
end behavioral;

