----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    14:25:11 11/10/2014 
-- design name: 
-- module name:    sumador_segmentado_4s2 - rtl 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity sumador_segmentado_4s2 is
	port(
		clk, rst, en	: in std_logic;
		a,b			: in std_logic_vector(7 downto 0);
		s				: out std_logic_vector(7 downto 0);
		cin	: in std_logic;
		cout : out std_logic
	);
end sumador_segmentado_4s2;

architecture rtl of sumador_segmentado_4s2 is

	-- component declaration
	component full_adder is
		port(
			a			: in std_logic;
			b			: in std_logic;
			cin		: in std_logic;
			s			: out std_logic;
			cout	: out std_logic
		);
	end component full_adder;
	
	-- signal declarations
	signal ff_1, ff_2, ff_3 : std_logic_vector(15 downto 0);
	signal c_1, c_2, c_3	: std_logic;
	signal carr_1, carr_2, carr_3 : std_logic_vector(4 downto 0);
	
	
	signal ff_a, ff_b : std_logic_vector(7 downto 4);
	signal ff_s				: std_logic_vector(3 downto 0);
	signal ff_c			  : std_logic;
	signal carry_a		: std_logic_vector(4 downto 0);
	signal carry_b		: std_logic_vector(8 downto 4);
	
	signal input, output : std_logic_vector(7 downto 0);
	signal carry : std_logic_vector(1 downto 0);
	
begin
	
	primera_fila : for i in 0 to 3 generate
		i_full_adder : full_adder
			port map(
				a			=> a(i),
				b			=> b(i),
				cin		=> carry_a(i),
				s			=> ff_s(i),
				cout	=> carry_a(i+1)
			);
	end generate primera_fila;
	
	segunda_fila : for i in 4 to 7 generate
		i_full_adder : full_adder
			port map(
				a			=> ff_a(i),
				b			=> ff_b(i),
				cin		=> carry_b(i),
				s			=> s(i),
				cout	=> carry_b(i+1)
			);
	end generate segunda_fila;

	tercera_fila : for i in 0 to 3 generate
		i_full_adder : full_adder
			port map(
				a			=> a(i),
				b			=> b(i),
				cin		=> carry_a(i),
				s			=> ff_s(i),
				cout	=> carry_a(i+1)
			);
	end generate tercera_fila;
	
	cuarta_fila : for i in 4 to 7 generate
		i_full_adder : full_adder
			port map(
				a			=> ff_a(i),
				b			=> ff_b(i),
				cin		=> carry_b(i),
				s			=> s(i),
				cout	=> carry_b(i+1)
			);
	end generate cuarta_fila;
	
	
	p_ff : process(clk, rst)
	begin
		if rst='1' then
			ff_a <= (others => '0');
			ff_b <= (others => '0');
			s(3 downto 0) <= (others => '0');
			ff_c <= '0';
		elsif rising_edge(clk) and en='1' then
			ff_a(7 downto 4) <= a(7 downto 4);
			ff_b(7 downto 4) <= b(7 downto 4);
			s(3 downto 0) <= ff_s(3 downto 0);
			ff_c <= carry_a(4);
		end if;
	end process p_ff;

end rtl;

