----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    20:46:08 11/13/2014 
-- design name: 
-- module name:    serial_bit_detector - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity serial_bit_detector is
	generic(
		g_filter_width : natural := 12
	);
	port(
		clk			: in std_logic;
		rst			: in std_logic;
		listen		: in std_logic;
		s_clk			: in std_logic;
		s_data		: in std_logic;
		value			: out std_logic;
		bit_recv		: out std_logic
	);
end serial_bit_detector;

architecture behavioral of serial_bit_detector is

	-- type definitions
	type shift_filter_array is array(0 to g_filter_width-1) of std_logic;
	
	-- signal declarations
	signal shift_clk, shift_data : shift_filter_array;

begin

	p_shift : process(clk, rst)
	begin
		if rst='1' then
			for i in 0 to g_filter_width-1 loop
				shift_clk(i) <= '1';
				shift_data(i) <= '1';
			end loop;
		elsif rising_edge(clk) and listen='1' then
			shift_clk(0) <= s_clk;
			shift_data(0) <= s_data;
			for i in 1 to g_filter_width-1 loop
				shift_clk(i) <= shift_clk(i-1);
				shift_data(i) <= shift_data(i-1);
			end loop;		
		end if;
	end process p_shift;
	
	p_bit_recv : process(shift_clk)
		variable aux_val1, aux_val2 : std_logic;
	begin
		aux_val1 := '1';
		aux_val2 := '0';
		for i in 0 to g_filter_width/2-1 loop
			aux_val1 := aux_val1 and shift_clk(i);
			aux_val2 := aux_val2 or shift_clk(g_filter_width/2+i);
		end loop;
		if  aux_val1='1' and aux_val2='0' then
			bit_recv <= '1';
		else
			bit_recv <= '0';
		end if;
	end process p_bit_recv;
	
	p_value : process(shift_data)
		variable aux_val : std_logic;
	begin
		aux_val := '1';
		for i in g_filter_width/2 to g_filter_width-1 loop
			aux_val := aux_val and shift_data(i);
		end loop;
		value <= aux_val;
	end process p_value;

end behavioral;

