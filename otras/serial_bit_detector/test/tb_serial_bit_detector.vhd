----------------------------------------------------------------------------------
-- company: 
-- engineer:  daniel pinto rivero
-- 
-- create date:    10:04:28 10/24/2014 
-- design name: 
-- module name:    tb_adder_8b - testbench 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_serial_bit_detector is
end tb_serial_bit_detector;

architecture testbench of tb_serial_bit_detector is

	-- component declaration
	component serial_bit_detector is
		generic(
			g_filter_width : natural := 12
		);
		port(
			clk			: in std_logic;
			rst			: in std_logic;
			listen		: in std_logic;
			s_clk			: in std_logic;
			s_data		: in std_logic;
			value			: out std_logic;
			bit_recv		: out std_logic
		);
	end component serial_bit_detector;

	
	-- constant declaratinos
	constant clk_period : time := 10 us;
	constant s_clk_period	: time := clk_period*20;
	
	-- signal declarations
	signal clk, rst : std_logic;
	signal listen, s_clk, s_data, bit_recv : std_logic;
	signal value : std_logic;
	
begin
	i_dut : serial_bit_detector
		generic map(
			g_filter_width => 12
		)
		port map(
			clk			=> clk,
			rst			=> rst,
			listen		=> listen,
			s_clk		=> s_clk,
			s_data	=> s_data,
			value		=> value,
			bit_recv	=> bit_recv
		);
	
	p_rst : process
	begin
		rst <= '1';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	p_s_clk : process
	begin
		for i in 0 to 10 loop
			s_clk <= '1';
			wait for s_clk_period/2;
			s_clk <= '0';
			wait for s_clk_period/2;
		end loop;
		wait;
	end process p_s_clk;
	
	p_stim : process
		--variable data : std_logic;
	begin
		listen <= '1';
		s_data <= '1';
		
		-- start bit
		wait until falling_edge(s_clk);
		s_data <= '0';
		
		--data := '1';
		for i in 0 to 7 loop
			wait until falling_edge(s_clk);
			s_data <= not s_data;
		end loop;
		
		-- parity bit
		wait until falling_edge(s_clk);
		s_data <= '1';

		-- stop bit
		wait until falling_edge(s_clk);
		s_data <= '0';
	
		wait;
	end process p_stim;

end testbench;

