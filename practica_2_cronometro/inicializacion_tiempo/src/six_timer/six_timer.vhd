----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    22:16:39 11/11/2014 
-- design name: 
-- module name:    six_timer - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity six_timer is
	port(
		clk					: in std_logic;
		rst					: in std_logic;
		en					: in std_logic;
		up					: in std_logic;
		stop				: in std_logic;
		restart			: in std_logic;
		ini_sec_uni	: in std_logic_vector(3 downto 0);
		ini_sec_dec	: in std_logic_vector(3 downto 0);
		ini_min_uni	: in std_logic_vector(3 downto 0);
		ini_min_dec	: in std_logic_vector(3 downto 0);
		cnt_sec_uni	: out std_logic_vector(3 downto 0);
		cnt_sec_dec	: out std_logic_vector(3 downto 0);
		cnt_min_uni	: out std_logic_vector(3 downto 0);
		cnt_min_dec : out std_logic_vector(3 downto 0);
		end_reached: out std_logic
	);
end six_timer;

architecture behavioral of six_timer is

	-- component declarations
	component timer_load is
		generic(
			g_width : natural := 4
		);
		port(
			clk				: in std_logic;
			en				: in std_logic;
			rst				: in std_logic;
			restart		: in std_logic;
			up				: in std_logic;
			ini_value	: in std_logic_vector(g_width-1 downto 0);
			count			: out std_logic_vector(g_width-1 downto 0)
		);
	end component timer_load;

	-- type definitions
	type control_type is array(0 to 3) of std_logic;
	type value_type is array(0 to 3) of std_logic_vector(3 downto 0);
	
	-- signal declarations
	signal timer_restart : control_type;
	signal timer_enable : control_type;
	signal timer_ini_val  : value_type;
	signal timer_count		: value_type;
	
begin
		cnt_sec_uni <= timer_count(0);
		cnt_sec_dec <= timer_count(1);
		cnt_min_uni <= timer_count(2);
		cnt_min_dec <= timer_count(3);
	

	-- 4 timers de 4 bits para las unidades y decenas de los segundos y minutos
	timers : for i in 0 to 3 generate
		i_sec_uni : timer_load
			generic map(
				g_width => 4
			)
			port map(
				clk				=> clk,
				en				=> timer_enable(i),
				rst				=> rst,
				restart		=> timer_restart(i),
				up				=> up,
				ini_value	=> timer_ini_val(i),
				count			=> timer_count(i)
			);
		end generate timers;

	-- señales de enable de los timers
	p_enable : process(en, up, restart, stop, timer_count)
	begin
		for i in 0 to 3 loop
			timer_enable(i) <= '0';
		end loop;
		if en = '1' then
			timer_enable(0) <= '1';
			if restart='1' then
				for i in 0 to 3 loop
					timer_enable(i) <= '1';
				end loop;		
			elsif stop = '1' then
				for i in 0 to 3 loop
					timer_enable(i) <= '0';
				end loop;
			elsif up='1' then
				if timer_count(3) = "0101" and timer_count(2) = "1001" and timer_count(1) ="0101" and timer_count(0) = "1001" then
					for i in 0 to 3 loop
						timer_enable(i) <= '0';
					end loop;				
				else
					if timer_count(0) = "1001" then
						timer_enable(1) <= '1';
					end if;
					if timer_count(0) = "1001" and timer_count(1) = "0101" then
						timer_enable(2) <= '1';
					end if;
					if timer_count(2) = "1001" and timer_count(1) ="0101" and timer_count(0) = "1001" then
						timer_enable(3) <= '1';
					end if;
				end if;
			else
				if timer_count(3) = "0000" and timer_count(2) = "0000" and timer_count(1) ="0000" and timer_count(0) = "0000" then
					for i in 0 to 3 loop
						timer_enable(i) <= '0';
					end loop;				
				else
					if timer_count(0) = "0000" then
						timer_enable(1) <= '1';
					end if;
					if timer_count(0) = "0000" and timer_count(1) = "0000" then
						timer_enable(2) <= '1';
					end if;
					if timer_count(2) = "0000" and timer_count(1) ="0000" and timer_count(0) = "0000" then
						timer_enable(3) <= '1';
					end if;
				end if;
			end if;
		end if;
	end process p_enable;

	-- señales de restart de los timers
	p_restart : process(restart, up, timer_count)
	begin
		if restart = '1' then
			for i in 0 to 3 loop
				timer_restart(i) <= '1';
			end loop;
		else
			for i in 0 to 3 loop
				timer_restart(i) <= '0';
			end loop;
			if up = '1' then
				if timer_count(0) = "1001" then
					timer_restart(0) <= '1';
				end if;
				if timer_count(1) = "0101" then
					timer_restart(1) <= '1';
				end if;
				if timer_count(2) = "1001" then
					timer_restart(2) <= '1';
				end if;
				if timer_count(3) = "0101" then
					timer_restart(3) <= '1';
				end if;
			else
				for i in 0 to 3 loop
					if timer_count(i) = "0000" then
						timer_restart(i) <= '1';
					end if;
				end loop;
			end if;
		end if;	
	end process p_restart;
	
	-- Señales de inicio de los timers
	p_ini_value : process(restart, up, ini_sec_uni, ini_sec_dec, ini_min_uni, ini_min_dec)
	begin
		if restart = '1' then
			timer_ini_val(0) <= ini_sec_uni;
			timer_ini_val(1) <= ini_sec_dec;
			timer_ini_val(2) <= ini_min_uni;
			timer_ini_val(3) <= ini_min_dec;
		else
			if up = '1' then
				timer_ini_val(0) <= (others => '0');
				timer_ini_val(1) <= (others => '0');
				timer_ini_val(2) <= (others => '0');
				timer_ini_val(3) <= (others => '0');
			else
				timer_ini_val(0) <= "1001";
				timer_ini_val(1) <= "0101";
				timer_ini_val(2) <= "1001";
				timer_ini_val(3) <= "0101";		
			end if;
		end if;		
	end process p_ini_value;
	
	p_end_reached : process(up, timer_count)
	begin
		end_reached <= '0';
		if (up= '1' and timer_count(0) = "1001" and timer_count(1) = "0101" and timer_count(2) = "1001" and timer_count(3) = "0101") or
			(up= '0' and timer_count(0) = "0000" and timer_count(1) = "0000" and timer_count(2) = "0000" and timer_count(3) = "0000") then
			end_reached <= '1';
		end if;
	end process p_end_reached;
	
end behavioral;

