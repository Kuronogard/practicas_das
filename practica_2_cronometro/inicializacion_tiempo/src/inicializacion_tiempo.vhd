----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    12:49:56 11/08/2014 
-- design name: 
-- module name:    contador_ascendente - behavioral 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity inicializacion_tiempo is
	port(
		clk					: in std_logic;
		rst					: in std_logic;
		stop				: in std_logic;
		restart			: in std_logic;
		up					: in std_logic;
		show_secs   : in std_logic;
		initial_value	: in std_logic_vector(3 downto 0);
		button			: in std_logic_vector(3 downto 0);
		digit_out		: out std_logic_vector(7 downto 0);
		display_sel	: out std_logic_vector(3 downto 0)
	);
end inicializacion_tiempo;

architecture behavioral of inicializacion_tiempo is

	-- component declarations
	component d7segm4_driver is
		port(
			clk						: in std_logic;
			rst						: in std_logic;
			en						: in std_Logic;
			show					: in std_logic;
			display_0			: in std_logic_vector(7 downto 0);
			display_1			: in std_logic_vector(7 downto 0);
			display_2			: in std_logic_vector(7 downto 0);
			display_3			: in std_logic_vector(7 downto 0);
			display_in			: out std_logic_vector(7 downto 0);
			display_sel		: out std_logic_vector(3 downto 0)
		);
	end component d7segm4_driver;
	
	component freqdiv_50m250h is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			clk_1h		: out std_logic;
			clk_250h	: out std_logic
		);
	end component freqdiv_50m250h;

	component six_timer is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			en					: in std_logic;
			up					: in std_logic;
			stop				: in std_logic;
			restart			: in std_logic;
			ini_sec_uni	: in std_logic_vector(3 downto 0);
			ini_sec_dec	: in std_logic_vector(3 downto 0);
			ini_min_uni	: in std_logic_vector(3 downto 0);
			ini_min_dec	: in std_logic_vector(3 downto 0);
			cnt_sec_uni	: out std_logic_vector(3 downto 0);
			cnt_sec_dec	: out std_logic_vector(3 downto 0);
			cnt_min_uni	: out std_logic_vector(3 downto 0);
			cnt_min_dec : out std_logic_vector(3 downto 0);
			end_reached: out std_logic
		);
	end component six_timer;	

	component bcd_to_d7seg is
		port(
			input		: in std_logic_vector(3 downto 0);
			output	: out std_logic_vector(7 downto 0)
		);
	end component bcd_to_d7seg;
	
	component jkflipflop is
		port( 
				clk		: in std_logic;
				rst		: in std_logic;
				en		: in std_logic;
				j			: in  std_logic;
				k			: in  std_logic;
				q			: out std_logic
				);
	end component jkflipflop;

	-- type definitions
	type value_array is array(0 to 3) of std_logic_vector(3 downto 0);
	type d7seg_digit_array is array(0 to 3) of std_logic_vector(7 downto 0);

	-- signal declarations
	signal clk_250h, clk_1h : std_logic;
	signal end_reached, parpadeo : std_logic;
	signal digit, ini_value : value_array;
	signal d7seg_digit : d7seg_digit_array;
	signal display_0, display_1, display_2, display_3 : std_logic_vector(7 downto 0);

begin

	bcd_conv : for i in 0 to 3 generate
		i_bcd_to_d7seg : bcd_to_d7seg
			port map(
				input		=> digit(i),
				output	=> d7seg_digit(i)
			);
	end generate bcd_conv;

	i_d7seg_driver : d7segm4_driver
		port map(
			clk						=> clk,
			rst						=> rst,
			en						=> clk_250h,  -- fps*4
			show					=> parpadeo,
			display_0			=> display_0,
			display_1			=> display_1,
			display_2			=> display_2,
			display_3			=> display_3,
			display_in			=> digit_out,
			display_sel		=> display_sel
		);

	i_clk_250h : freqdiv_50m250h
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> '1',
			clk_1h		=> clk_1h,
			clk_250h	=> clk_250h
		);
		
--	i_parpadeo : jkflipflop
--		port map( 
--				clk		=> clk,
--				rst		=> rst,
--				en		=> clk_1h,
--				j			=> '1',
--				k			=> '1',
--				q			=> parpadeo
--				);

	i_time : six_timer
		port map(
			clk					=> clk,
			rst					=> rst,
			en					=> clk_1h,
			up					=> up,
			stop				=> stop,
			restart			=> restart,
			ini_sec_uni	=> ini_value(0),
			ini_sec_dec	=> ini_value(1),
			ini_min_uni	=> ini_value(2),
			ini_min_dec	=> ini_value(3),
			cnt_sec_uni	=> digit(0),
			cnt_sec_dec	=> digit(1),
			cnt_min_uni	=> digit(2),
			cnt_min_dec => digit(3),
			end_reached => end_reached
		);

		p_parpadeo : process(clk, rst)
		begin
			if rst='1' then
				parpadeo <= '0';
			elsif rising_edge(clk) then
				if end_reached = '0' then
					parpadeo <= '1';
				elsif clk_1h = '1' then
					parpadeo <= not parpadeo;
				end if;
			end if;
		end process p_parpadeo;

		p_ini_value : process(clk, rst, up)
		begin
			if rst='1' then
				if up='1' then
					ini_value(0) <= (others => '0');
					ini_value(1) <= (others => '0');
					ini_value(2) <= (others => '0');
					ini_value(3) <= (others => '0');
				else
					ini_value(0) <= "1001";
					ini_value(1) <= "0101";
					ini_value(2) <= "1001";
					ini_value(3) <= "0101";
				end if;
			elsif rising_edge(clk) then
				if button(0) = '1' then
					ini_value(0) <= initial_value;
				end if;
				if button(1) = '1' then
					ini_value(1) <= initial_value;
				end if;
				if button(2) = '1' then
					ini_value(2) <= initial_value;
				end if;
				if button(3) = '1' then
					ini_value(3) <= initial_value;
				end if;
			end if;
		end process p_ini_value;
		
		p_show_seconds : process(show_secs, d7seg_digit)
		begin
			if show_secs='1' then
				display_0 <= d7seg_digit(0);
				display_1 <= d7seg_digit(1);
				display_2 <= d7seg_digit(2);
				display_3 <= d7seg_digit(3);
			else
				display_0 <= d7seg_digit(2);
				display_1 <= d7seg_digit(3);
				display_2 <= "10111111";
				display_3 <= "10111111";
			end if;
		end process p_show_seconds;

end behavioral;

