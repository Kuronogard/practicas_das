----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    00:57:03 10/26/2014 
-- design name: 
-- module name:    tb_freq_div_100mho1h - testbench 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity tb_timer_six is
end tb_timer_six;

architecture testbench of tb_timer_six is

	-- component declaration
	component six_timer is
	port(
		clk					: in std_logic;
		rst					: in std_logic;
		en					: in std_logic;
		up					: in std_logic;
		stop				: in std_logic;
		restart			: in std_logic;
		ini_sec_uni	: in std_logic_vector(3 downto 0);
		ini_sec_dec	: in std_logic_vector(3 downto 0);
		ini_min_uni	: in std_logic_vector(3 downto 0);
		ini_min_dec	: in std_logic_vector(3 downto 0);
		cnt_sec_uni	: out std_logic_vector(3 downto 0);
		cnt_sec_dec	: out std_logic_vector(3 downto 0);
		cnt_min_uni	: out std_logic_vector(3 downto 0);
		cnt_min_dec : out std_logic_vector(3 downto 0)
	);
	end component six_timer;

	-- type declarations
	type value_type is array(0 to 3) of std_logic_vector(3 downto 0);

	-- constant declaratinos
	constant clk_period : time := 10 ns;
	
	-- signal declarations
	signal clk, rst, en, up, stop : std_logic;
	signal restart : std_logic;
	signal ini_value, timer_count : value_type;
	
	
begin

	i_dut : six_timer
		port map(
			clk					=> clk,
			rst					=> rst,
			en					=> en,
			up					=> up,
			stop				=> stop,
			restart			=> restart,
			ini_sec_uni	=> ini_value(0),
			ini_sec_dec	=> ini_value(1),
			ini_min_uni	=> ini_value(2),
			ini_min_dec	=> ini_value(3),
			cnt_sec_uni	=> timer_count(0),
			cnt_sec_dec	=> timer_count(1),
			cnt_min_uni	=> timer_count(2),
			cnt_min_dec => timer_count(3)
		);

	p_rst : process
	begin
		rst <= '1';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	p_en : process
	begin
		en <= '1';
		wait for clk_period;
		en <= '0';
		wait for clk_period*3;
	end process p_en;
	
	p_stim : process
	begin
		for i in 0 to 3 loop
			ini_value(i) <= (others=> '0');
		end loop;
		up <= '1';
		stop <= '0';
		restart <= '0';
		
		wait for clk_period*100;
		wait until falling_edge(clk);
		up <= '0';
		stop <= '0';
		restart <= '0';	
	
		wait for clk_period*40;
		wait until falling_edge(clk);
		up <= '0';
		stop <= '0';
		restart <= '0';	

		wait for clk_period*20;
		wait until falling_edge(clk);
		up <= '0';
		stop <= '1';
		restart <= '0';			

		wait for clk_period*60;
		wait until falling_edge(clk);
		up <= '0';
		stop <= '0';
		restart <= '0';			
		
		wait for clk_period*60;
		wait until falling_edge(clk);
		ini_value(0) <= "1000";
		ini_value(1) <= "0010";
		ini_value(2) <= "0010";
		ini_value(3) <= "0001";
		up <= '0';
		stop <= '0';
		restart <= '1';
		
		wait for clk_period*10;
		wait until falling_edge(clk);
		up <= '0';
		stop <= '0';
		restart <= '0';	
		
		wait;
	end process p_stim;


end testbench;

