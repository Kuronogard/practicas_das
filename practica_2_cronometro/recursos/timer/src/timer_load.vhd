----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    21:25:40 11/11/2014 
-- design name: 
-- module name:    timer - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity timer_load is
	generic(
		g_width : natural := 4
	);
	port(
		clk		: in std_logic;
		en		: in std_logic;
		rst		: in std_logic;
		restart	: in std_logic;
		up		: in std_logic;
		ini_value	: in std_logic_vector(g_width-1 downto 0);
		count			: out std_logic_vector(g_width-1 downto 0)
	);
end timer_load;

architecture behavioral of timer_load is

	-- component declarations
	component adder is
		generic(
			g_width		: natural := 8
			);
		port(
			op_a			: in std_logic_vector(g_width-1 downto 0);
			op_b			: in std_logic_vector(g_width-1 downto 0);
			carr_in		: in std_logic;
			result		   : out std_logic_vector(g_width-1 downto 0)
			);
	end component adder;

	-- signal declarations
	signal current_count : std_logic_vector(g_width-1 downto 0);
	signal next_add	: std_logic_vector(g_width-1 downto 0);
	signal add_result	: std_logic_vector(g_width-1 downto 0);

begin

	count <= current_count;

	i_add : adder
		generic map(
			g_width => g_width
			)
		port map(
			op_a			=> current_count,
			op_b			=> next_add,
			carr_in		=> '0',
			result		   => add_result
			);
			
			p_next_add : process(up)
			begin
				if up = '1' then
					next_add <= "0001";
				else
					next_add <= "1111";
				end if;
			end process p_next_add;
			
			p_timer : process(clk, rst)
			begin
				if rst='1' then
					current_count <= (others => '0');
				elsif rising_edge(clk) then
					if restart = '1' then
						current_count <= ini_value;
					elsif en='1' then
						current_count <= add_result;
					end if;
				end if;
			end process p_timer;

end behavioral;

