----------------------------------------------------------------------------------
-- company: 
-- engineer:  daniel pinto rivero
-- 
-- create date:    10:04:28 10/24/2014 
-- design name: 
-- module name:    tb_adder_8b - testbench 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_timer is
end tb_timer;

architecture testbench of tb_timer is

	-- component declaration
	component timer_load is
		generic(
			g_width : natural := 4
		);
		port(
			clk		: in std_logic;
			en		: in std_logic;
			rst		: in std_logic;
			restart	: in std_logic;
			up		: in std_logic;
			ini_value	: in std_logic_vector(g_width-1 downto 0);
			count			: out std_logic_vector(g_width-1 downto 0)
		);
	end component timer_load;

	
	-- constant declaratinos
	constant clk_period : time := 10 us;
	
	-- signal declarations
	signal clk, rst, en : std_logic;
	signal restart, up : std_logic;
	signal count : std_logic_vector(3 downto 0);
	
begin
	i_dut : timer_load
		generic map(
			g_width => 4
		)
		port map(
			clk				=> clk,
			en				=> en,
			rst				=> rst,
			restart		=> restart,
			up				=> up,
			ini_value	=> "1001",
			count			=> count
		);
	
	p_rst : process
	begin
		rst <= '1';
		en  <= '0';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		en  <= '1';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	
	p_stim : process
	begin
		up <= '1';
		restart <= '0';
		wait for clk_period*3;
		wait until falling_edge(clk);
		wait for 6*clk_period;
		up <= '0';
		wait for clk_period*4;
		restart <= '1';
		wait;
	end process p_stim;

end testbench;

