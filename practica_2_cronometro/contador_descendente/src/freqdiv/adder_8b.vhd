----------------------------------------------------------------------------------
-- author:		Daniel Pinto Rivero
-- create date:		09:42:44 10/24/2014 
-- module name:	adder_8b - behavioral 
-- project name: 
-- target devices: 	spartan-3
-- description:   8 bit adder
-- 	
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity adder_8b is
	port(
		op_a			: in std_logic_vector(7 downto 0);
		op_b			: in std_logic_vector(7 downto 0);
		carr_in		: in std_logic;
		result		   : out std_logic_vector(7 downto 0);
		carr_out	: out std_logic
		);
end adder_8b;

architecture behavioral of adder_8b is
begin

	
	p_result : process(op_a, op_b, carr_in)
		variable carry : std_logic_vector(8 downto 0);
	begin
		carry(0) := carr_in;
		for i in 0 to 7 loop
			carry(i+1) := ( op_a(i) and op_b(i) ) or ( op_a(i) and carry(i) ) or ( op_b(i) and carry(i) );
			result(i) <= op_a(i) xor op_b(i) xor carry(i);
		end loop;
		carr_out <= carry(8);
	end process p_result;


end behavioral;

