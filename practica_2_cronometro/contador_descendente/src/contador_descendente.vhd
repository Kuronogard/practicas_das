----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    12:49:56 11/08/2014 
-- design name: 
-- module name:    contador_ascendente - behavioral 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity contador_descendente is
	port(
		clk					: in std_logic;
		rst					: in std_logic;
		stop				: in std_logic;
		restart			: in std_logic;
		show_secs   : in std_logic;
		digit_out		: out std_logic_vector(7 downto 0);
		display_sel	: out std_logic_vector(3 downto 0)
	);
end contador_descendente;

architecture behavioral of contador_descendente is

	-- component declarations
	component d7segm4_driver is
		port(
			clk						: in std_logic;
			rst						: in std_logic;
			en						: in std_Logic;
			display_0			: in std_logic_vector(7 downto 0);
			display_1			: in std_logic_vector(7 downto 0);
			display_2			: in std_logic_vector(7 downto 0);
			display_3			: in std_logic_vector(7 downto 0);
			display_in			: out std_logic_vector(7 downto 0);
			display_sel		: out std_logic_vector(3 downto 0)
		);
	end component d7segm4_driver;
	
	component freqdiv_50m250h is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			clk_1h		: out std_logic;
			clk_250h	: out std_logic
		);
	end component freqdiv_50m250h;

	component six_timer is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			en					: in std_logic;
			up					: in std_logic;
			stop				: in std_logic;
			restart			: in std_logic;
			ini_sec_uni	: in std_logic_vector(3 downto 0);
			ini_sec_dec	: in std_logic_vector(3 downto 0);
			ini_min_uni	: in std_logic_vector(3 downto 0);
			ini_min_dec	: in std_logic_vector(3 downto 0);
			cnt_sec_uni	: out std_logic_vector(3 downto 0);
			cnt_sec_dec	: out std_logic_vector(3 downto 0);
			cnt_min_uni	: out std_logic_vector(3 downto 0);
			cnt_min_dec : out std_logic_vector(3 downto 0)
		);
	end component six_timer;	

	component bcd_to_d7seg is
		port(
			input		: in std_logic_vector(3 downto 0);
			output	: out std_logic_vector(7 downto 0)
		);
	end component bcd_to_d7seg;

	-- type definitions
	type digit_array is array(0 to 3) of std_logic_vector(3 downto 0);
	type d7seg_digit_array is array(0 to 3) of std_logic_vector(7 downto 0);

	-- signal declarations
	signal clk_250h, clk_1h : std_logic;
	signal digit : digit_array;
	signal d7seg_digit : d7seg_digit_array;
	signal display_0, display_1, display_2, display_3 : std_logic_vector(7 downto 0);

begin

	bcd_conv : for i in 0 to 3 generate
		i_bcd_to_d7seg : bcd_to_d7seg
			port map(
				input		=> digit(i),
				output	=> d7seg_digit(i)
			);
	end generate bcd_conv;

	i_d7seg_driver : d7segm4_driver
		port map(
			clk						=> clk,
			rst						=> rst,
			en						=> clk_250h,  -- fps*4
			display_0			=> display_0,
			display_1			=> display_1,
			display_2			=> display_2,
			display_3			=> display_3,
			display_in			=> digit_out,
			display_sel		=> display_sel
		);

	i_clk_250h : freqdiv_50m250h
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> '1',
			clk_1h		=> clk_1h,
			clk_250h	=> clk_250h
		);

	i_seconds : six_timer
		port map(
			clk					=> clk,
			rst					=> rst,
			en					=> clk_1h,
			up					=> '0',
			stop				=> stop,
			restart			=> restart,
			ini_sec_uni	=> "1001",
			ini_sec_dec	=> "0101",
			ini_min_uni	=> "1001",
			ini_min_dec	=> "0101",
			cnt_sec_uni	=> digit(0),
			cnt_sec_dec	=> digit(1),
			cnt_min_uni	=> digit(2),
			cnt_min_dec => digit(3)
		);

		
		p_show_seconds : process(show_secs, d7seg_digit)
		begin
			if show_secs='1' then
				display_0 <= d7seg_digit(0);
				display_1 <= d7seg_digit(1);
				display_2 <= d7seg_digit(2);
				display_3 <= d7seg_digit(3);
			else
				display_0 <= d7seg_digit(2);
				display_1 <= d7seg_digit(3);
				display_2 <= "10111111";
				display_3 <= "10111111";
			end if;
		end process p_show_seconds;

end behavioral;

