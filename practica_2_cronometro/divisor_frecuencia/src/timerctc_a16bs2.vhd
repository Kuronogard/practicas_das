----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    18:18:24 10/24/2014 
-- design name: 
-- module name:    counter - behavioral 
-- project name: 
-- target devices: 
--
-- while en='1', counts to "max_count" and then reset to 0 to
-- keep counting
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity timerctc_a16bs2 is
	port(
		clk       : in std_logic;
		rst       : in std_logic;
		en        : in std_logic;
		max_count : in std_logic_vector(15 downto 0);
		reached   : out std_logic
	);
end timerctc_a16bs2;

architecture behavioral of timerctc_a16bs2 is

	-- component declaration
	component adder_16bs2_i is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			op_a			: in std_logic_vector(15 downto 0);
			result		: out std_logic_vector(15 downto 0)
			);
	end component adder_16bs2_i;
	
	component reg is
		generic( 
			g_width : natural := 8
			);
		port( 
			clk : in     std_logic;
			d   : in     std_logic_vector (g_width - 1 downto 0);
			en  : in     std_logic;
			rst : in     std_logic;
			q   : out    std_logic_vector (g_width - 1 downto 0)
			);
	end component reg;
	
	component comparator is
		generic(
			g_width : natural := 8
		);
		port(
			op_a : in std_logic_vector(g_width-1 downto 0);
			op_b : in std_logic_vector(g_width-1 downto 0);
			result: out std_logic
		);
	end component comparator;
	
	--signal declaration
	signal current_count, add_output, next_count : std_logic_vector(15 downto 0);
	signal comp, max_reached : std_logic;
	signal comp_zero : std_logic;

begin
	
	i_add : adder_16bs2_i
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> en,
			op_a			=> current_count,
			result		=> add_output
			);

	i_curr_count : reg
		generic map( 
			g_width => 16
			)
		port map( 
			clk 		=> clk,
			d   		=> next_count,
			en  	=> en,
			rst 		=> rst,
			q   		=> current_count
			);

	i_comparator : comparator
		generic map(
			g_width => 16
		)
		port map(
			op_a 		=> next_count,
			op_b 		=> max_count,
			result		=> comp
		);	
		
	i_comparator_zero : comparator
		generic map(
			g_width => 16
		)
		port map(
			op_a 		=> current_count,
			op_b 		=> "0000000000000000",
			result		=> comp_zero
		);	
	
	p_next_count : process(add_output, max_reached, comp_zero)
	begin
		if max_reached='1'then
			next_count <= (others => '0');
		elsif comp_zero='1' then
			next_count <= ( 0 => '1', others=> '0');
		else
			next_count <= add_output;
		end if;
	end process p_next_count;

	
	p_max_reached : process(clk, rst)
	begin
		if rst='1' then
			max_reached <= '0';
		elsif rising_edge(clk) then
			if en='1' then
				max_reached <= comp;
			end if;
		end if;
	end process p_max_reached;
	
	reached <= max_reached;
	
end behavioral;

