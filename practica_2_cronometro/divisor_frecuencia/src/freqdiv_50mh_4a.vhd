----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    10:59:59 11/08/2014 
-- design name: 
-- module name:    freqdiv_50mh_4a - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity freqdiv_50mh_4a is
	port(
		clk				: in std_logic;
		rst				: in std_logic;
		en				: in std_logic;
		clk_100k	: out std_logic;
		clk_100h	: out std_logic;
		clk_2h		: out std_logic;
		clk_1h		: out std_logic
	);
end freqdiv_50mh_4a;

architecture behavioral of freqdiv_50mh_4a is

	-- component declarations
	component timerctc_a16bs2 is
		port(
			clk       			: in std_logic;
			rst      			: in std_logic;
			en       			: in std_logic;
			max_count 	: in std_logic_vector(15 downto 0);
			reached   	: out std_logic
		);
	end component timerctc_a16bs2;

	component timerctc_f8b is
		port(
			clk      			: in std_logic;
			rst       			: in std_logic;
			en        		: in std_logic;
			max_count 	: in std_logic_vector(7 downto 0);
			reached   	: out std_logic
		);
	end component timerctc_f8b;

	component jkflipflop is
		port( 
				clk		: in std_logic;
				rst		: in std_logic;
				en		: in std_logic;
				j			: in  std_logic;
				k			: in  std_logic;
				q			: out std_logic
				);
	end component jkflipflop;

	component rise_edge_detector is
		port(
			clk				: in std_logic;
			rst	   		: in std_logic;
			en				: in std_logic;
			signal_in	: in std_logic;
			edge			: out std_logic
		);
	end component rise_edge_detector;


	--signal declaratinos
	signal reached : std_logic_vector(3 downto 0);
	signal reach_edge : std_logic_vector(3 downto 0);

begin

	clk_100k <= reach_edge(0);
	clk_100h <= reach_edge(1);
	clk_2h	 <= reach_edge(2);
	clk_1h	 <= reach_edge(3);
	
	edge : for i in 0 to 3 generate
		i_rise_edge : rise_edge_detector
			port map(
				clk				=> clk,
				rst	   		=> rst,
				en				=> en,
				signal_in	=> reached(i),
				edge			=> reach_edge(i)
			);
	end generate edge;

	i_100k : timerctc_a16bs2
		port map(
			clk       			=> clk,
			rst      			=> rst,
			en       			=> en,
			max_count 	=> "0000000111110011",   -- 500-1
			reached   	=> reached(0)
		);
		
	i_100h : timerctc_a16bs2
		port map(
			clk       			=> clk,
			rst      			=> rst,
			en       			=> reach_edge(0),
			max_count 	=> "0000001111100111",   -- 1000-1
			reached   	=> reached(1)
		);

	i_2h : timerctc_f8b
		port map(
			clk      			=> clk,
			rst       			=> rst,
			en        		=> reach_edge(1),
			max_count 	=> "00110001",						-- 50-1
			reached   	=> reached(2)
		);

	i_1h : jkflipflop
		port map( 
				clk		=> clk,
				rst		=> rst,
				en		=> reach_edge(2),
				j			=> '1',
				k			=> '1',
				q			=> reached(3)
				);

	
end behavioral;

