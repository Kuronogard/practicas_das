----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    09:39:15 10/27/2014 
-- design name: 
-- module name:    req_register - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity req_register is
	port(
		clk			   : in std_logic;
		rst			   : in std_logic;
		en			   : in std_logic;
		clear_req  : in std_logic_vector(3 downto 0);
		req_in	   : in std_logic_vector(3 downto 0);
		req_out	   : out std_logic_vector(3 downto 0)
	);
end req_register;

architecture behavioral of req_register is
	signal next_request : std_logic_vector(3 downto 0);
	signal request : std_logic_vector(3 downto 0);
begin
	
	req_out <= request;
	p_req_out : process(clk, rst)
	begin
		if rst='1' then
			request <= (others => '0');
		elsif rising_edge(clk) then
			if en='1' then
				request <= next_request;
			end if;
		end if;
	end process p_req_out;

	-- como vamos a trabajar con un reloj tan lento en->1Hz, este diseño es suficiente para
	-- trabajar con rebotes
	p_next_request : process(req_in, request, clear_req)
	begin
		for i in 0 to 3 loop
			next_request(i) <= (not req_in(i))  or ( request(i)  and (not clear_req(i)) ) ;
		end loop;
	end process p_next_request;

end behavioral;

