----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    10:12:03 10/27/2014 
-- design name: 
-- module name:    puertas - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity puertas is
	port(
		clk		         : in std_logic;
		rst		         : in std_logic;
		en		         : in std_logic;
		open_gates	: in std_logic;
		gates_state	: out std_logic;
		gates				: out std_logic_vector(6 downto 0)
	);
end puertas;

architecture behavioral of puertas is
	
	-- type declarations
	type state_type is ( op0_st, op1_st, open_st, cl0_st, cl1_st, closed_st);
	
	-- signal declaration
	signal current_state, next_state : state_type;
	
begin

	p_current_state : process(clk, rst)
	begin
		if rst='1' then
			current_state <= closed_st;
		elsif rising_edge(clk) then
			if en='1' then
				current_state <= next_state;
			end if;
		end if;
	end process p_current_state;

	p_next_state : process(current_state, open_gates)
	begin
		case current_state is
			when op0_st =>
				next_state <= op1_st;
			when op1_st =>
				next_state <= open_st;
			when open_st =>
				if open_gates='1' then
					next_state <= open_st;
				else
					next_state <= cl0_st;
				end if;
			when cl0_st =>
				next_state <= cl1_st;
			when cl1_st =>
				next_state <= closed_st;
			when closed_st =>
				if open_gates='1' then
					next_state <= op0_st;
				else
					next_state <= closed_st;
				end if;
			end case;
	end process p_next_state;
	
	p_outputs : process(current_state)
	begin
		case current_state is
			when op0_st 	 =>	
				gates_state <= '0';
				gates <= "1100011";		-- estado de los leds
			when op1_st 	 =>	
				gates_state <= '0';
				gates <= "1000001";
			when open_st   =>	
				gates_state <= '0';
				gates <= "0000000";
			when cl0_st 	 =>	
				gates_state <= '0';
				gates <= "1100011";
			when cl1_st 	 =>	
				gates_state <= '0';
				gates <= "1110111";
			when closed_st =>	
				gates_state <= '1';
				gates <= "1111111";
		end case;
	end process p_outputs;

end behavioral;

