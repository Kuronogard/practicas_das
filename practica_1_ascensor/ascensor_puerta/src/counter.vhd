----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    18:10:03 10/25/2014 
-- design name: 
-- module name:    counter - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity counter is
	generic(
		g_width : natural := 8
	);
	port(
		clk		  : in std_logic;
		rst		  : in std_logic;
		en		  : in std_logic;
		restart  : in std_logic;
		count		: out std_logic_vector(g_width-1 downto 0)
	);
end counter;

architecture behavioral of counter is
	
	-- component declarations
	component jkflipflop is
		port( 
				clk		: in std_logic;
				rst		: in std_logic;
				en		: in std_logic;
				j			: in  std_logic;
				k			: in  std_logic;
				q			: out std_logic
				);
	end component jkflipflop;
	
	-- signal declarations
	signal ff_input_j : std_logic_vector(g_width-1 downto 0);
	signal ff_input_k : std_logic_vector(g_width-1 downto 0);
	signal count_int : std_logic_vector(g_width-1 downto 0);
	
begin

	count <= count_int;

	ff_array : for i in 0 to g_width-1 generate
		i_jkff : jkflipflop
			port map(
					clk		=> clk,
					rst		=> rst,
					en		=> en,
					j			=> ff_input_j(i),
					k			=> ff_input_k(i),
					q			=> count_int(i)
					);
	end generate ff_array;


	p_ff_input : process(count_int, restart)
		variable aux_j_in : std_logic_vector(g_width-1 downto 0);
		variable aux_k_in : std_logic_vector(g_width-1 downto 0);
		variable aux_and_acum : std_logic;
	begin
		aux_j_in(0)  := not restart;
		aux_k_in(0) := '1';
		for i in 1 to g_width-1 loop
			aux_j_in(i)  := aux_j_in(i-1) and count_int(i-1);
			aux_k_in(i) := aux_j_in(i) or restart;
		end loop;
		ff_input_k <= aux_k_in;
		ff_input_j  <= aux_j_in;
	end process p_ff_input;



end behavioral;

