----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    09:34:19 10/27/2014 
-- design name: 
-- module name:    ascensor - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity ascensor is
	port(
		clk		      : in std_logic;
		rst		      : in std_logic;
		req_in	   : in std_logic_vector(3 downto 0);
		req_out	   : out std_logic_vector(3 downto 0);
		floor			: out std_logic_vector(1 downto 0);
		led_test		: out std_logic
	);
end ascensor;

architecture behavioral of ascensor is
	
	-- component declaration
	component freq_div_100mho1h is
		port(
			clk		   : in std_logic;
			rst		   : in std_logic;
			en		   : in std_logic;
			clk_div	: out std_logic
			);
	end component freq_div_100mho1h;
	
	component jkflipflop is
		port( 
			clk		: in std_logic;
			rst		: in std_logic;
			en		: in std_logic;
			j			: in  std_logic;
			k			: in  std_logic;
			q			: out std_logic
			);
	end component jkflipflop;
	
	-- type definition
	type state_type is ( pt0_st, pt1_st, pt2_st, pt3_st );
	
	-- signal declaration
	signal current_state, next_state : state_type; 
	signal request : std_logic_vector(3 downto 0);
	signal en : std_logic;

begin
	
	i_freq_div : freq_div_100mho1h
		port map(
			clk		   => clk,
			rst		   => rst,
			en		   	=> '1',
			clk_div	   => en
			);

	-- jkflipflop para probar l�a frecuencia del reloj en un led
	i_led : jkflipflop
		port map( 
         clk		=> clk,
         rst		=> rst,
         en		   => '1',
			j			=> en,
			k			=> en,
         q			=> led_test
			);

	p_request : process(req_in)
	begin
		for i in 0 to 3 loop
			request(i) <= not req_in(i);
		end loop;
	end process p_request;
	req_out <= request;

	p_current_state : process(clk, rst)
	begin
		if rst='1' then
			current_state <= pt0_st;
		elsif rising_edge(clk) then
			if en='1' then
				current_state <= next_state;
			end if;
		end if;
	end process p_current_state;

	p_next_state : process(current_state, request)
	begin
		case current_state is
			when pt0_st =>
				if request(1) = '1' or request(2)='1' or request(3)='1' then
					next_state <= pt1_st;
				else
					next_state <= pt0_st;
				end if;
			when pt1_st =>
				if request(2)='1' or request(3)='1' then
					next_state <= pt2_st;
				elsif request(0)='1' then
					next_state <= pt0_st;
				else
					next_state <= pt1_st;
				end if;
			when pt2_st =>
				if request(3)='1' then
					next_state <= pt3_st;
				elsif request(0)='1' or request(1)='1' then
					next_state <= pt1_st;
				else
					next_state <= pt2_st;
				end if;
			when pt3_st =>
				if request(0)='1' or request(1)='1' or request(2)='1' then
					next_state <= pt2_st;
				else
					next_state <= pt3_st;
				end if;
			end case;
	end process p_next_state;

	p_outputs : process(current_state)
	begin
		case current_state is
			when pt0_st =>		floor <= "00";
			when pt1_st =>		floor <= "01";
			when pt2_st =>		floor <= "10";
			when pt3_st =>		floor <= "11";
		end case;
	end process p_outputs;
	
end behavioral;

