----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    00:41:03 10/26/2014 
-- design name: 
-- module name:    freq_div_100mho1h - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity freq_div_100mho1h is
	port(
		clk		   : in std_logic;
		rst		   : in std_logic;
		en		   : in std_logic;
		clk_div	: out std_logic
		);
end freq_div_100mho1h;

architecture behavioral of freq_div_100mho1h is

	-- component declaration
	component counter_ctc_2 is
		port(
			clk       : in std_logic;
			rst       : in std_logic;
			en        : in std_logic;
			max_count : in std_logic_vector(7 downto 0);
			reached   : out std_logic
		);
	end component counter_ctc_2;

	-- signal declaration
	signal clk_div_int : std_logic_vector(2 downto 0);

begin

	i_div_0 : counter_ctc_2
		port map(
			clk       => clk,
			rst       => rst,
			en        => en,
			max_count => "01100011",   -- 100-1
			reached   => clk_div_int(0)
		);
		
	i_div_1 : counter_ctc_2
		port map(
			clk       => clk,
			rst       => rst,
			en        => clk_div_int(0),
			max_count => "01100011",   -- 100-1
			reached   => clk_div_int(1)
		);
		
	i_div_2 : counter_ctc_2
		port map(
			clk       => clk,
			rst       => rst,
			en        => clk_div_int(1),
			max_count => "01100011",   -- 100-1
			reached   => clk_div_int(2)
		);
		
	i_div_3 : counter_ctc_2
		port map(
			clk       => clk,
			rst       => rst,
			en        => clk_div_int(2),
			max_count => "01100011",   -- 100-1
			reached   => clk_div
		);

end behavioral;

