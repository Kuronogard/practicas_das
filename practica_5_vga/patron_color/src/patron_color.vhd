----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    18:22:14 11/24/2014 
-- design name: 
-- module name:    patron_color - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity patron_color is
	port(
		clk				: in std_logic;
		rst				: in std_logic;
		hsync			: out std_logic;
		vsync			: out std_logic;
		vga_red		: out std_logic_vector(2 downto 0);
		vga_green	: out std_logic_vector(2 downto 0);
		vga_blue		: out std_logic_vector(1 downto 0)
		);
end patron_color;

architecture behavioral of patron_color is

	-- component declarations
	component vga_driver_640x480 is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			hsync				: out std_logic;
			vsync				: out std_logic;
			h_addr			: out std_logic_vector(15 downto 0);
			v_addr			: out std_logic_vector(15 downto 0)
		);
	end component vga_driver_640x480;
	

	-- signal declarations
	signal vga_vaddr, vga_haddr : std_logic_vector(15 downto 0);

begin

	i_vga_driver : vga_driver_640x480
		port map(
			clk					=> clk,
			rst					=> rst,
			hsync				=> hsync,
			vsync				=> vsync,
			h_addr			=> vga_haddr,
			v_addr			=> vga_vaddr
		);
		

		p_image : process(vga_haddr, vga_vaddr)
		begin
			vga_red <= "000";
			vga_green <= "000";
			vga_blue <= "00";

			-- valores perfectos para imprimir vga_vaddr(34-513)   vga_haddr(144-783)
			if to_integer(unsigned(vga_vaddr)) >= 35  and to_integer(unsigned(vga_vaddr)) <= 37 and to_integer(unsigned(vga_haddr)) >= 144 and  to_integer(unsigned(vga_haddr)) <= 783 then
				vga_red <= "000";
				vga_green <= "010";
				vga_blue <= "01";
			elsif to_integer(unsigned(vga_vaddr)) >= 510  and to_integer(unsigned(vga_vaddr)) <= 512 and to_integer(unsigned(vga_haddr)) >= 144 and  to_integer(unsigned(vga_haddr)) <= 783 then
				vga_red <= "000";
				vga_green <= "010";
				vga_blue <= "01";
			elsif to_integer(unsigned(vga_vaddr)) >= 33  and to_integer(unsigned(vga_vaddr)) <= 513 and to_integer(unsigned(vga_haddr)) >= 145 and  to_integer(unsigned(vga_haddr)) <= 147 then
				vga_red <= "000";
				vga_green <= "010";
				vga_blue <= "01";
			elsif to_integer(unsigned(vga_vaddr)) >= 33  and to_integer(unsigned(vga_vaddr)) <= 513 and to_integer(unsigned(vga_haddr)) >= 780 and  to_integer(unsigned(vga_haddr)) <= 782 then
				vga_red <= "000";
				vga_green <= "010";
				vga_blue <= "01";
			end if;
			
			if to_integer(unsigned(vga_haddr)) >= 454  and to_integer(unsigned(vga_haddr)) <= 464 and to_integer(unsigned(vga_vaddr)) >= 263 and  to_integer(unsigned(vga_vaddr)) <= 273 then
				vga_red(2 downto 0) <=  rom_img(to_integer(unsigned(vga_vaddr)))(to_integer(unsigned(vga_haddr)))(7 downto 5);
				vga_green(2 downto 0) <=  rom_img(to_integer(unsigned(vga_vaddr)))(to_integer(unsigned(vga_haddr)))(4 downto 2);
				vga_blue(1 downto 0) <=  rom_img(to_integer(unsigned(vga_vaddr)))(to_integer(unsigned(vga_haddr)))(1 downto 0);
			end if;
			
		end process p_image;

end behavioral;

