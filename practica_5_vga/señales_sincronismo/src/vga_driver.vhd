----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    21:33:04 11/21/2014 
-- design name: 
-- module name:    vga_driver - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity vga_driver is
	port(
		clk					: in std_logic;
		rst					: in std_logic;
		hsync				: out std_logic;
		vsync				: out std_logic;
		vga_red		: out std_logic_vector(2 downto 0);
		vga_green	: out std_logic_vector(2 downto 0);
		vga_blue		: out std_logic_vector(1 downto 0)
	);
end vga_driver;

architecture behavioral of vga_driver is


	-- component declarations
	component freqdiv_50m25m is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			clk_25m	: out std_logic
		);
	end component freqdiv_50m25m;
	
	component counter_a16bs2 is
		port(
			clk       : in std_logic;
			rst       : in std_logic;
			en        : in std_logic;
			restart	: in std_logic;
			count		: out std_logic_vector(15 downto 0)
		);
	end component counter_a16bs2;
	
	component rise_edge_detector is
		port(
			clk				: in std_logic;
			rst	   		: in std_logic;
			en				: in std_logic;
			signal_in	: in std_logic;
			edge			: out std_logic
		);
	end component rise_edge_detector;

	-- signal declarations
	signal clk_25m, hsync_restart, vsync_restart, vsync_en : std_logic;
	signal hsync_count, vsync_count : std_logic_vector(15 downto 0);

begin
	vga_red <= (others => '0');
	vga_green <= (others => '0');
	vga_blue <= (others => '0');

	i_clk25m : freqdiv_50m25m
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> '1',
			clk_25m	=> clk_25m
		);
		
	i_vsync_en : rise_edge_detector
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> '1',
			signal_in	=> hsync_restart,
			edge			=> vsync_en
		);
		
	i_hsync_count : counter_a16bs2
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> clk_25m,
			restart		=> hsync_restart,
			count			=> hsync_count
		);
		
	i_vsync_count : counter_a16bs2
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> vsync_en,
			restart		=> vsync_restart,
			count			=> vsync_count
		);

	p_sync : process(clk, rst)
	begin
		if rst='1' then
			hsync <= '0';
			vsync <= '0';
		elsif rising_edge(clk) and clk_25m='1' then
				if hsync_count = "0000000001011111" then       --96-1
					hsync <= '1';
				elsif hsync_count = "0000001100011111" then   --800-1
					hsync <= '0';
				end if;
				
				if vsync_count = "0000000000000001" then          --2-1
					vsync <= '1';
				elsif vsync_count = "0000001000001001" then   --521-1
					vsync <= '0';
				end if;
		end if;
	end process p_sync;

	p_control : process(vsync_count, hsync_count)
	begin
		hsync_restart <= '0';
		vsync_restart <= '0';
		if hsync_count = "0000001100011111" then   --800-1
			hsync_restart <= '1';
		end if;
		if vsync_count = "0000001000001001" then   --521-1
			vsync_restart <= '1';
		end if;	
	end process p_control;

end behavioral;

