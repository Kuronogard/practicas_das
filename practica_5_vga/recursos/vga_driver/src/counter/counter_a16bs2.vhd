----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    18:18:24 10/24/2014 
-- design name: 
-- module name:    counter - behavioral 
-- project name: 
-- target devices: 
--
-- while en='1', counts to "max_count" and then reset to 0 to
-- keep counting
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity counter_a16bs2 is
	port(
		clk       : in std_logic;
		rst       : in std_logic;
		en        : in std_logic;
		restart	: in std_logic;
		count		: out std_logic_vector(15 downto 0)
	);
end counter_a16bs2;

architecture behavioral of counter_a16bs2 is

	-- component declaration
	component adder_16bs2_i2 is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			restart		: in std_logic;
			op_a			: in std_logic_vector(15 downto 0);
			result		: out std_logic_vector(15 downto 0)
			);
	end component adder_16bs2_i2;
	
	--signal declaration
	signal add_output : std_logic_vector(15 downto 0);

begin
	
	i_add : adder_16bs2_i2
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> en,
			restart		=> restart,
			op_a			=> add_output,
			result		=> add_output
			);
			
	p_current_count : process(clk, rst)
	begin
		if rst='1' then
			count <= (others => '0');
		elsif rising_edge(clk) and en='1' then
			if restart = '1' then
				count <= (others => '0');
			else
				count <= add_output;
			end if;
		end if;
	end process p_current_count;
	
end behavioral;

