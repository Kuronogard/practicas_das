----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    21:32:01 10/27/2014 
-- design name: 
-- module name:    freq_div_100m_4a - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity freqdiv_50m25m is
	port(
		clk				: in std_logic;
		rst				: in std_logic;
		en				: in std_logic;
		clk_25m	: out std_logic
	);
end freqdiv_50m25m;

architecture behavioral of freqdiv_50m25m is

	-- component declaration
	component jkflipflop is
		port( 
				clk		: in std_logic;
				rst		: in std_logic;
				en		: in std_logic;
				j			: in  std_logic;
				k			: in  std_logic;
				q			: out std_logic
				);
	end component jkflipflop;

	component rise_edge_detector is
		port(
			clk				: in std_logic;
			rst	   		: in std_logic;
			en				: in std_logic;
			signal_in	: in std_logic;
			edge			: out std_logic
		);
	end component rise_edge_detector;

	-- signal declaration
		signal max_reached, freqs : std_logic;

begin

	clk_25m  <= freqs;
	
	i_rise_detector : rise_edge_detector
		port map(
			clk				=> clk,
			rst	   		=> rst,
			en				=> '1',
			signal_in	=> max_reached,
			edge			=> freqs
		);
	
	i_clk_25m : jkflipflop
		port map( 
				clk				=> clk,
				rst	   		=> rst,
				en				=> en,
				j					=> '1',
				k					=> '1',
				q					=> max_reached
			);
	

end behavioral;

