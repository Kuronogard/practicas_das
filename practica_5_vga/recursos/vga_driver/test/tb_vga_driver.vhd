----------------------------------------------------------------------------------
-- company: 
-- engineer:  daniel pinto rivero
-- 
-- create date:    10:04:28 10/24/2014 
-- design name: 
-- module name:    tb_adder_8b - testbench 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_vga_driver is
end tb_vga_driver;

architecture testbench of tb_vga_driver is

	-- component declaration
	component vga_driver is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			hsync				: out std_logic;
			vsync				: out std_logic;
			vga_red		: out std_logic_vector(2 downto 0);
			vga_green	: out std_logic_vector(2 downto 0);
			vga_blue		: out std_logic_vector(1 downto 0)
		);
	end component vga_driver;

	
	-- constant declaratinos
	constant clk_period : time := 10 us;
	
	-- signal declarations
	signal clk, rst : std_logic;
	signal hsync, vsync : std_logic;
	signal vga_red, vga_green : std_logic_vector(2 downto 0);
	signal vga_blue : std_logic_vector(1 downto 0);

	
begin
	
	
	i_dut : vga_driver
		port map(
			clk					=> clk,
			rst					=> rst,
			hsync				=> hsync,
			vsync				=> vsync,
			vga_red		=> vga_red,
			vga_green	=> vga_green,
			vga_blue		=> vga_blue
		);
	
	
	p_rst : process
	begin
		rst <= '1';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;

end testbench;

