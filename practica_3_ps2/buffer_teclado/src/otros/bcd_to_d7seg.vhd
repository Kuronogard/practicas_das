----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    16:47:02 10/31/2014 
-- design name: 
-- module name:    bcd_conversor - struct 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity bcd_to_d7seg is
	port(
		input		: in std_logic_vector(3 downto 0);
		output	: out std_logic_vector(7 downto 0)
	);
end bcd_to_d7seg;

architecture struct of bcd_to_d7seg is
begin

	p_output : process(input)
	begin
		case input is
			when "0000" => output <= "11000000";  -- 0
			when "0001" => output <= "11111001";  -- 1
			when "0010" => output <= "10100100";  -- 2
			when "0011" => output <= "10110000";  -- 3
			when "0100" => output <= "10011001";  -- 4
			when "0101" => output <= "10010010";  -- 5
			when "0110" => output <= "10000010";  -- 6 
			when "0111" => output <= "11111000";  -- 7
			when "1000" => output <= "10000000";  -- 8
			when "1001" => output <= "10010000";  -- 9
			when "1010" => output <= "10001000";  -- a
			when "1011" => output <= "10000011";  -- b
			when "1100" => output <= "11000110";  -- c
			when "1101" => output <= "10100001";  -- d
			when "1110" => output <= "10000110";  -- e
			when "1111" => output <= "10001110";  -- f
			when others  => output <= "11111111";  -- none
		end case;
	end process p_output;

end struct;

