----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    21:32:01 10/27/2014 
-- design name: 
-- module name:    freq_div_100m_4a - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity freqdiv_50m250h is
	port(
		clk				: in std_logic;
		rst				: in std_logic;
		en				: in std_logic;
		clk_250h	: out std_logic
	);
end freqdiv_50m250h;

architecture behavioral of freqdiv_50m250h is

	-- component declaration
	component timerctc_a16bs2 is
		port(
			clk       : in std_logic;
			rst       : in std_logic;
			en        : in std_logic;
			max_count : in std_logic_vector(15 downto 0);
			reached   : out std_logic
		);
	end component timerctc_a16bs2;

	component timerctc_f is
		generic(
			g_width : natural := 8
		);
		port(
			clk       			: in std_logic;
			rst       			: in std_logic;
			en        		: in std_logic;
			max_count	: in std_logic_vector(g_width-1 downto 0);
			reached   	: out std_logic
		);
	end component timerctc_f;

	component timerctc_f8b is
		port(
			clk       			: in std_logic;
			rst       			: in std_logic;
			en        		: in std_logic;
			max_count	: in std_logic_vector(7 downto 0);
			reached   	: out std_logic
		);
	end component timerctc_f8b;

	component rise_edge_detector is
		port(
			clk				: in std_logic;
			rst	   		: in std_logic;
			en				: in std_logic;
			signal_in	: in std_logic;
			edge			: out std_logic
		);
	end component rise_edge_detector;

	-- signal declaration
		signal max_reached : std_logic_vector(1 downto 0);

begin
	
	i_rise_detector : rise_edge_detector
		port map(
			clk				=> clk,
			rst	   		=> rst,
			en				=> '1',
			signal_in	=> max_reached(1),
			edge			=> clk_250h
		);
	
	i_timer_1k : timerctc_a16bs2
		port map(
			clk        		=> clk,
			rst       		=> rst,
			en        		=> en,
			max_count => "1100001101001111",   -- 50000-1
			reached   	=> max_reached(0)
		);

	i_timer_250h : timerctc_f
		generic map(
			g_width => 2
		)
		port map(
			clk       			=> clk,
			rst       			=> rst,
			en        		=> max_reached(0),
			max_count	=> "11",							-- 4-1
			reached   	=> max_reached(1)
		);

end behavioral;

