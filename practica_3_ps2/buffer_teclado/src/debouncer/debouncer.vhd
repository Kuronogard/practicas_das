----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    20:46:08 11/13/2014 
-- design name: 
-- module name:    serial_bit_detector - behavioral 
-- project name: 
----------------------------------------------------------------------------------
-- description:
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity debouncer is
	port(
		clk							: in std_logic;
		rst							: in std_logic;
		button_in				: in std_logic;
		button_out_rise	: out std_logic
	);
end debouncer;

architecture behavioral of debouncer is

	-- component declarations
	component counter_f is
		generic(
			g_width : natural := 8
		);
		port(
			clk		  : in std_logic;
			rst		  : in std_logic;
			en		  : in std_logic;
			restart  : in std_logic;
			count		: out std_logic_vector(g_width-1 downto 0)
		);
	end component counter_f;
	
	component rise_edge_detector is
		port(
			clk				: in std_logic;
			rst	   		: in std_logic;
			en				: in std_logic;
			signal_in	: in std_logic;
			edge			: out std_logic
		);
	end component rise_edge_detector;
	
	-- signal declarations
	signal button_deb, button_deb_restart : std_logic;
	signal button_deb_count : std_logic_vector(7 downto 0);
	

begin

	i_rise_out : rise_edge_detector
		port map(
			clk		  		=> clk,
			rst		  		=> rst,
			en		  		=> '1',
			signal_in	=> button_deb,
			edge			=> button_out_rise
		);
	
	i_buttton_deb : counter_f
		generic map(
			g_width => 8
		)
		port map(
			clk		  => clk,
			rst		  => rst,
			en		  => '1',
			restart  => button_deb_restart,
			count		=> button_deb_count
		);

	p_deb_outputs : process(clk, rst)
	begin
		if rst='1' then
			button_deb <= '0';
			button_deb_restart <= '0';
		elsif rising_edge(clk) then
			button_deb_restart <= '0';
			if button_in = button_deb then
				button_deb_restart <= '1';
			end if;
			if button_deb_count = "11111111" then
				button_deb <= not button_deb;
				button_deb_restart <= '1';
			end if;
		end if;
	end process p_deb_outputs;

end behavioral;

