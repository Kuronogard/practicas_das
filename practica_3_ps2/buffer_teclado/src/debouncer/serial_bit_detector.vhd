----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    20:46:08 11/13/2014 
-- design name: 
-- module name:    serial_bit_detector - behavioral 
-- project name: 
----------------------------------------------------------------------------------
-- description:
-- 
-- Debouncer para las señales clk y data del puerto serie "ps/2", lee las señales de entrada a la frecuencia
-- de clk y va contando, ejemplo:
-- si la última salida debounceada de data fue '0', va contando los '1's de la entrada de data
-- cuando se alcance la cuenta máxima, se invierte la salida y se reinicia la cuenta
-- si mientras cuenta '0's, se detecta un '1', la cuenta también se reinicia y viceversa.
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity serial_bit_detector is
	generic(
		g_filter_delay : natural := 15
	);
	port(
		clk					: in std_logic;
		rst					: in std_logic;
		s_clk				: in std_logic;
		s_data			: in std_logic;
		s_clk_out		: out std_logic;
		s_data_out	: out std_logic
	);
end serial_bit_detector;

architecture behavioral of serial_bit_detector is

	-- component declarations
	component counter_f is
		generic(
			g_width : natural := 8
		);
		port(
			clk		  : in std_logic;
			rst		  : in std_logic;
			en		  : in std_logic;
			restart  : in std_logic;
			count		: out std_logic_vector(g_width-1 downto 0)
		);
	end component counter_f;
	
	-- signal declarations
	signal s_clk_deb, s_data_deb, data_deb_restart, clk_deb_restart : std_logic;
	signal clk_deb_count, data_deb_count : std_logic_vector(4 downto 0);
	

begin
	
	i_data_deb_count : counter_f
		generic map(
			g_width => 5
		)
		port map(
			clk		  => clk,
			rst		  => rst,
			en		  => '1',
			restart  => data_deb_restart,
			count		=> data_deb_count
		);

	i_clk_deb_count : counter_f
		generic map(
			g_width => 5
		)
		port map(
			clk		  => clk,
			rst		  => rst,
			en		  => '1',
			restart  => clk_deb_restart,
			count		=> clk_deb_count
		);

	p_deb_outputs : process(clk, rst)
	begin
		if rst='1' then
			s_clk_deb <= '1';    --s_clk;
			s_data_deb <= '1';   --s_data;
			data_deb_restart <= '0';
			clk_deb_restart <= '0';
		elsif rising_edge(clk) then
			data_deb_restart <= '0';
			clk_deb_restart <= '0';
			if s_clk = s_clk_deb then
				clk_deb_restart <= '1';
			end if;
			if s_data = s_data_deb then
				data_deb_restart <= '1';
			end if;
			if data_deb_count = "11111" then
				s_data_deb <= not s_data_deb;
				data_deb_restart <= '1';
			end if;
			if clk_deb_count = "11111" then
				s_clk_deb <= not s_clk_deb;
				clk_deb_restart <= '1';
			end if;
		end if;
	end process p_deb_outputs;
	
	s_clk_out <= s_clk_deb;
	s_data_out <= s_data_deb;

--	s_clk_out <= s_clk;
--	s_data_out <= s_data;
end behavioral;

