----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    17:45:40 11/20/2014 
-- design name: 
-- module name:    detector_tecla - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity buffer_teclado is
	port(
		clk							: in std_logic;
		rst							: in std_logic;
		s_data					: in std_logic;
		s_clk						: in std_logic;
		siguiente				: in std_logic;
		ps2_buff_empty	: out std_logic;
		ps2_buff_full		: out std_logic;
		digit_out				: out std_logic_vector(7 downto 0);
		display_sel			: out std_logic_vector(3 downto 0)
	);
end buffer_teclado;

architecture behavioral of buffer_teclado is

	
	-- component declarations
	component fifo_circular is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			write_en		: in std_logic;
			read_en		: in std_logic;
			write_value	: in std_logic_vector(7 downto 0);
			read_value	: out std_logic_vector(7 downto 0);
			full					: out std_logic;
			empty			: out std_logic
		);
	end component fifo_circular;
	
	component freqdiv_50m250h is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			clk_250h	: out std_logic
		);
	end component freqdiv_50m250h;

	component bcd_to_d7seg is
		port(
			input		: in std_logic_vector(3 downto 0);
			output	: out std_logic_vector(7 downto 0)
		);
	end component bcd_to_d7seg;

	component d7segm4_driver is
		port(
			clk						: in std_logic;
			rst						: in std_logic;
			en						: in std_Logic;
			show					: in std_logic;
			display_0			: in std_logic_vector(7 downto 0);
			display_1			: in std_logic_vector(7 downto 0);
			display_2			: in std_logic_vector(7 downto 0);
			display_3			: in std_logic_vector(7 downto 0);
			display_in			: out std_logic_vector(7 downto 0);
			display_sel		: out std_logic_vector(3 downto 0)
		);
	end component d7segm4_driver;
	
	component ps2_interface_driver is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			s_clk				: in std_logic;
			s_data			: in std_logic;
			data_ready	: out std_logic;
			read_byte		: out std_logic_vector(7 downto 0)
		);
	end component ps2_interface_driver;
	
	component debouncer is
		port(
			clk							: in std_logic;
			rst							: in std_logic;
			button_in				: in std_logic;
			button_out_rise	: out std_logic
		);
	end component debouncer;
	
	-- signal declarations
	signal clk_250h : std_logic;
	signal display_0, display_1 : std_logic_vector(7 downto 0);
	signal fifo_write_en, fifo_read_en : std_logic;
	signal fifo_write, fifo_read : std_logic_vector(7 downto 0);

begin
	---------------------------------------------------------------------------------------------------------
	-- controlador para mostrar los datos en los d7seg
	---------------------------------------------------------------------------------------------------------
	i_clk250h : freqdiv_50m250h
		port map(
			clk					=> clk,
			rst					=> rst,
			en					=> '1',
			clk_250h		=> clk_250h
		);

	i_7seg_display_0 : bcd_to_d7seg
		port map(
			input		=> fifo_read(3 downto 0),
			output	=> display_0
		);

	i_7seg_display_1 : bcd_to_d7seg
		port map(
			input		=> fifo_read(7 downto 4),
			output	=> display_1
		);

	i_d7seg_driver : d7segm4_driver
		port map(
			clk					=> clk,
			rst					=> rst,
			en					=> clk_250h,
			show				=> '1',
			display_0		=> display_0,
			display_1		=> display_1,
			display_2		=> "00000000",
			display_3		=> "00000000",
			display_in		=> digit_out,
			display_sel	=> display_sel
		);
		
	---------------------------------------------------------------------------------------------------------
	-- El controlador de ps2 (escribe en la fifo cada vez que haya un dato listo)
	---------------------------------------------------------------------------------------------------------
	i_ps2_driver : ps2_interface_driver
		port map(
			clk					=> clk,
			rst					=> rst,
			s_clk				=> s_clk,
			s_data			=> s_data,
			data_ready	=> fifo_write_en,
			read_byte		=> fifo_write
		);
		
	---------------------------------------------------------------------------------------------------------
	-- El debouncer del botton "siguiente" (lee de la fifo cada vez que se detecte una pulsacion)
	---------------------------------------------------------------------------------------------------------
	i_button_deb : debouncer
		port map(
			clk							=> clk,
			rst							=> rst,
			button_in				=> siguiente,
			button_out_rise	=> fifo_read_en
		);
	

	---------------------------------------------------------------------------------------------------------
	-- la fifo para mediar entre el controlador de ps2 y el resto
	---------------------------------------------------------------------------------------------------------
	i_fifo_ps2 : fifo_circular
		port map(
			clk					=> clk,
			rst					=> rst,
			write_en		=> fifo_write_en,
			read_en		=> fifo_read_en,
			write_value	=> fifo_write,
			read_value	=> fifo_read,
			full					=> ps2_buff_full,
			empty			=> ps2_buff_empty
		);

end behavioral;

