----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    21:41:56 11/13/2014 
-- design name: 
-- module name:    ps2_interface_driver - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity ps2_interface_driver is
	port(
		clk			: in std_logic;
		rst			: in std_logic;
		s_clk			: in std_logic;
		s_data		: in std_logic;
		data_ready	: out std_logic;
		--busy			: out std_logic;
		--error			: out std_logic;
		read_byte	: out std_logic_vector(7 downto 0)
	);
end ps2_interface_driver;

architecture behavioral of ps2_interface_driver is

	---------------------------------------------------------------------------------------
	-- component declarations
	---------------------------------------------------------------------------------------
	component serial_bit_detector is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			s_clk				: in std_logic;
			s_data			: in std_logic;
			s_clk_out		: out std_logic;
			s_data_out	: out std_logic
		);
	end component serial_bit_detector;

	component counter_f is
		generic(
			g_width : natural := 8
		);
		port(
			clk		  : in std_logic;
			rst		  : in std_logic;
			en		  : in std_logic;
			restart  : in std_logic;
			count		: out std_logic_vector(g_width-1 downto 0)
		);
	end component counter_f;

	---------------------------------------------------------------------------------------
	-- type definitions
	---------------------------------------------------------------------------------------	
	type state_type is (idle_st, rx_read_st, rx_wait_rise_st, rx_wait_fall_st, rx_success_st, rx_error_st, rx_wait_release_st);
	
	---------------------------------------------------------------------------------------
	-- signal declarations
	---------------------------------------------------------------------------------------
	-- state machine
	signal current_state, next_state : state_type;
	
	
	-- debounced serial lines
	signal s_clk_int, s_data_int : std_logic;
	
	-- control signals
	signal parity_restart, parity_calc, parity_res : std_logic;
	signal frame_shift : std_logic;
	signal count_en, count_restart : std_logic;
	signal count_output : std_logic_vector(3 downto 0);
	
	-- data registers
	signal frame : std_logic_vector(7 downto 0);   -- (d0-d7)
	signal parity : std_logic;
		
begin

	-- filtro para la linea de reloj y la de datos
	i_bit_detector : serial_bit_detector
		port map(
			clk				 => clk,
			rst				 => rst,
			s_clk			 => s_clk,
			s_data		 => s_data,
			s_clk_out	 => s_clk_int,
			s_data_out => s_data_int
		);
	
	-- contador de bits (enviados o recibidos)
	i_bit_counter : counter_f
		generic map(
			g_width => 4
		)
		port map(
			clk		 	 => clk,
			rst		  	=> rst,
			en		  	=> count_en,
			restart => count_restart,
			count	=> count_output
		);
	
	
	-- shift register para almacenar el frame que se va a enviar o recibir
	-- bits de control: [frame_ld, frame_shift]
	p_frame : process(clk, rst)
	begin
		if rst='1' then
			frame <= (others => '0');
		elsif rising_edge(clk) then
			if frame_shift='1' then
				frame(7) <= s_data_int;
				frame(6 downto 0) <= frame(7 downto 1);
			end if;
		end if;
	end process p_frame;
	
	p_parity : process(clk, rst)
	begin
		if rst='1' then
			parity <= '0';
		elsif rising_edge(clk) then
			if parity_restart = '1' then
				parity <= '0';
			elsif parity_res='1' then
				parity <= not( (not parity) xor s_data_int );
			elsif parity_calc='1' then
				parity <= parity xor s_data_int;
			end if;
		end if;
	end process p_parity;
	
	-- =======================================================
	--		máquina de estados del driver (recepción)
	-- =======================================================
	p_current_state : process(clk, rst)
	begin
		if rst='1' then
			current_state <= idle_st;
			read_byte <= (others => '0');
		elsif rising_edge(clk) then
			current_state <= next_state;
			if current_state = rx_wait_release_st then
				read_byte <= frame(7 downto 0);
			end if;
		end if;
	end process p_current_state;
	
	-- idle_st, rx_read_st, rx_wait_rise_st, rx_wait_fall_st, rx_parity_check, rx_success_st, rx_error_st
	-- calcula el siguiente estado
	p_next_state : process(current_state, count_output, s_clk_int, s_data_int, parity)
	begin
		next_state <= current_state; 
		case current_state is
			------------------------------------------------------------------------------
			-- idle
			------------------------------------------------------------------------------
			when idle_st =>
				if s_clk_int='0' and s_data_int = '0' then						-- si se baja la linea de clk, se va a recibir un dato
					next_state <= rx_wait_rise_st;
				end if;
			------------------------------------------------------------------------------
			-- receive states
			------------------------------------------------------------------------------
			when rx_read_st =>
				next_state <= rx_wait_rise_st;
			when rx_wait_rise_st =>
				if s_clk_int = '1' then
					next_state <= rx_wait_fall_st;
				end if;
			when rx_wait_fall_st =>
				if s_clk_int = '0' then
					if count_output = "1001" then			-- "10" ha leido los 8 bits, paridad y stop
						next_state <= rx_wait_release_st;
					else
						next_state <= rx_read_st;
					end if;
				end if;
				
			when rx_wait_release_st =>
				if s_clk_int = '1' then
					next_state <= rx_error_st;
					if s_data_int = '1' then
						if parity = '1' then
							next_state <= rx_success_st;
						end if;
					end if;
				end if;
				
			when rx_error_st =>
				next_state <= idle_st;
			when rx_success_st =>
				next_state <= idle_st;
			------------------------------------------------------------------------------
			-- send states
			------------------------------------------------------------------------------

		end case;
	end process p_next_state;

	-- calcula las salidas en función del estado actual (máquina de moore)
	p_state_machine_outputs : process(current_state, count_output)
	begin
			--busy <= '1';
			data_ready <= '0';
			--error <= '0';
			frame_shift <= '0';
			parity_restart <= '0';
			parity_calc <= '0';
			parity_res <= '0';
			count_en <= '0';
			count_restart <= '0';
		
		case current_state is
			------------------------------------------------------------------------------
			-- idle
			------------------------------------------------------------------------------
			when idle_st =>
				count_restart <= '1';
				count_en <= '1';
				parity_restart <= '1';
				--busy <= '0';
			------------------------------------------------------------------------------
			-- receive outputs
			------------------------------------------------------------------------------
			when rx_read_st =>
				count_en <= '1';
				if count_output = "1000" then		-- ha leido 8 bits, calcular resultado de paridad
					parity_res <= '1';
				else														-- calculo parcial de paridad y guardar el valor en frame
					parity_calc <= '1';
					frame_shift <= '1';
				end if;
			when rx_wait_rise_st =>
			when rx_wait_fall_st =>
			when rx_error_st =>
				--error <= '1';
			when rx_success_st =>
				data_ready <= '1';
			when rx_wait_release_st =>
			------------------------------------------------------------------------------
			-- send outputs
			------------------------------------------------------------------------------

		end case;
	end process p_state_machine_outputs;

end behavioral;

