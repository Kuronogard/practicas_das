----------------------------------------------------------------------------------
-- company: 
-- engineer:  daniel pinto rivero
-- 
-- create date:    10:04:28 10/24/2014 
-- design name: 
-- module name:    tb_adder_8b - testbench 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_buffer_teclado is
end tb_buffer_teclado;

architecture testbench of tb_buffer_teclado is

	-- component declaration
	component buffer_teclado is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			s_data			: in std_logic;
			s_clk				: in std_logic;
			siguiente		: in std_logic;
			digit_out		: out std_logic_vector(7 downto 0);
			display_sel	: out std_logic_vector(3 downto 0)
		);
	end component buffer_teclado;

	
	-- constant declaratinos
	constant clk_period : time := 20 ns;
	constant s_clk_period	: time := clk_period*200;
	
	-- signal declarations
	signal clk, rst : std_logic;
	signal s_clk, s_data : std_logic;
	signal siguiente, send, send_finished : std_logic;
	signal data : std_logic_vector(7 downto 0);
	signal d7seg_digit : std_logic_vector(7 downto 0);
	signal d7seg_sel : std_logic_vector(3 downto 0);
	
begin

	i_dut : buffer_teclado
		port map(
			clk					=> clk,
			rst					=> rst,
			s_clk				=> s_clk,
			s_data			=> s_data,
			siguiente		=> siguiente,
			digit_out		=> d7seg_digit,
			display_sel	=> d7seg_sel
		);
	
	p_rst : process
	begin
		rst <= '1';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	p_s_clk : process
	begin
		send_finished <= '0';
		s_clk <= '1';
		wait until rst='0';
		wait for clk_period*5;
		loop
			wait until send = '1';
			wait until falling_edge(clk);
			for i in 0 to 11 loop
				wait for s_clk_period/2;
				s_clk <= '0';
				wait for s_clk_period/2;
				s_clk <= '1';
			end loop;
			send_finished <= '1';
			wait for clk_period;
			send_finished <= '0';
		end loop;
	end process p_s_clk;
	
	p_s_data : process
	begin
		s_data <= '1';
		loop
			wait until send= '1';
			wait until rising_edge(s_clk);			-- start bit		
			s_data <= '0';
			for i in 0 to 7 loop
				wait until rising_edge(s_clk);		-- data bits (7 downto 0)
				s_data <= data(i);
			end loop;
			wait until rising_edge(s_clk);				-- parity bit
			s_data <= not (data(0) xor data(1) xor data(2) xor data(3) xor data(4) xor data(5) xor data(6) xor data(7));
			wait until rising_edge(s_clk);				-- stop bit
			s_data <= '1';
		end loop;
	end process;
	
	p_stim : process
	begin
		send <= '0';
		siguiente <= '0';
		wait for clk_period*20;
		wait until falling_edge(clk);
		
		-- pedir un número (pulsar siguiente) con la fifo vacía
		siguiente <= '1';
		wait for clk_period;
		siguiente <= '0';
		wait for clk_period*10;
		
		-- recibir un valor mediante ps2 y pulsar "siguiente"
		data <= "00110101";
		send <= '1';
		wait for clk_period;
		send <= '0';
		wait until send_finished='1';
		wait for clk_period*2;
		wait until falling_edge(clk);
		siguiente <= '1';
		
		-- recibir 20 números mediante ps2  (0,2,4,6,8,10...)
		for i in 0 to 19 loop
			data <= std_logic_vector(to_unsigned(i*2, 8));
			send <= '1';
			wait for clk_period;
			send <= '0';
			wait until send_finished='1';
			wait until falling_edge(clk);
		end loop;
		
		wait for clk_period*20;
		-- pulsar 10 veces "siguiente"
		for i in 0 to 19 loop
			siguiente <= '1';
			wait for clk_period*50;
			siguiente <= '0';
			wait for clk_period*200;
		end loop;
		
		for i in 19 to 23 loop
			data <= std_logic_vector(to_unsigned(i*2, 8));
			send <= '1';
			wait for clk_period;
			send <= '0';
			wait until send_finished='1';
			wait until falling_edge(clk);
		end loop;
		
		wait for clk_period*20;
		
		-- pulsar 10 veces "siguiente"
		for i in 0 to 19 loop
			siguiente <= '1';
			wait for clk_period*50;
			siguiente <= '0';
			wait for clk_period*200;
		end loop;	

		wait;
	end process p_stim;

end testbench;

