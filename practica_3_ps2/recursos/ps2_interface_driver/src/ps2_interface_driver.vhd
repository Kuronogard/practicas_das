----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    21:41:56 11/13/2014 
-- design name: 
-- module name:    ps2_interface_driver - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity ps2_interface_driver is
	port(
		clk			: in std_logic;
		rst			: in std_logic;
		s_clk			: inout std_logic;
		s_data		: inout std_logic;
		start_write : in std_logic;
		write_byte : in std_logic_vector(7 downto 0);
		data_ready	: out std_logic;
		busy			: out std_logic;
		error			: out std_logic;
		read_byte	: out std_logic_vector(7 downto 0)
	);
end ps2_interface_driver;

architecture behavioral of ps2_interface_driver is

	---------------------------------------------------------------------------------------
	-- component declarations
	---------------------------------------------------------------------------------------
	component serial_bit_detector is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			s_clk				: in std_logic;
			s_data			: in std_logic;
			s_clk_out		: out std_logic;
			s_data_out	: out std_logic
		);
	end component serial_bit_detector;

	component counter_f is
		generic(
			g_width : natural := 8
		);
		port(
			clk		  : in std_logic;
			rst		  : in std_logic;
			en		  : in std_logic;
			restart  : in std_logic;
			count		: out std_logic_vector(g_width-1 downto 0)
		);
	end component counter_f;
	
	component counter_a16bs2 is
		port(
			clk       			: in std_logic;
			rst       			: in std_logic;
			en        		: in std_logic;
			restart			: in std_logic;
			max_count 	: in std_logic_vector(15 downto 0);
			reached   	: out std_logic
		);
	end component counter_a16bs2;

	---------------------------------------------------------------------------------------
	-- type definitions
	---------------------------------------------------------------------------------------	
	type state_type is (idle_st, wait_release_st, error_st, success_st, 
			rx_read_st, rx_wait_rise_st, rx_wait_fall_st, rx_par_check_st,
			tx_clk_down_st, tx_data_down_st, tx_release_clk_st, tx_next_data_st, tx_wait_rise_st, tx_wait_fall_st, 
			tx_par_wait_rise_st, tx_par_wait_fall_st,tx_release_data_st,  tx_wait_ack_st, tx_ack_check_st);
	
	---------------------------------------------------------------------------------------
	-- signal declarations
	---------------------------------------------------------------------------------------
	-- state machine
	signal current_state, next_state : state_type;
	
	
	-- debounced serial lines
	signal s_clk_int, s_data_int : std_logic;
	
	-- control signals
	signal parity_calc : std_logic;
	signal frame_shift : std_logic;
	signal count_en, count_restart : std_logic;
	signal count_output : std_logic_vector(3 downto 0);
	signal delay_en, delay_restart, delay_end : std_logic;
	signal delay_count, delay_max_count : std_logic_vector(15 downto 0);
	
	signal check_parity, check_ack : std_logic;
	-- data registers
	signal frame : std_logic_vector(7 downto 0);   -- (d0-d7)
	signal parity, success : std_logic;
	
	signal s_data_value, s_clk_value, s_data_write, s_clk_write : std_logic;
		
begin

	-- filtro para la linea de reloj y la de datos
	i_bit_detector : serial_bit_detector
		port map(
			clk				 => clk,
			rst				 => rst,
			s_clk			 => s_clk,
			s_data		 => s_data,
			s_clk_out	 => s_clk_int,
			s_data_out => s_data_int
		);
	
	-- contador de bits (enviados o recibidos)
	i_bit_counter : counter_f
		generic map(
			g_width => 4
		)
		port map(
			clk		 	 => clk,
			rst		  	=> rst,
			en		  	=> count_en,
			restart => count_restart,
			count	=> count_output
		);

	-- contador para hacer los delays
	i_delay : counter_a16bs2
		port map(
			clk       			=> clk,
			rst       			=> rst,
			en        		=> delay_en,
			restart			=> delay_restart,
			max_count 	=> delay_max_count,
			reached   	=> delay_end
		);
	
	-- puertos triestado
	s_clk <= s_clk_value when s_clk_write = '1' else 'Z';
	s_data <= s_data_value when s_data_write = '1' else 'Z';
	
	-- shift register para almacenar el frame que se va a enviar o recibir
	-- bits de control: [frame_ld, frame_shift]
	p_frame : process(clk, rst)
	begin
		if rst='1' then
			frame <= (others => '0');
		elsif rising_edge(clk) then
			if frame_shift='1' then
				frame(7) <= s_data_int;
				frame(6 downto 0) <= frame(7 downto 1);
			end if;
		end if;
	end process p_frame;
	
	p_parity : process(clk, rst)
	begin
		if rst='1' then
			parity <= '0';
		elsif rising_edge(clk) then
			if current_state = idle_st then			-- parity restart
				parity <= '0';
			elsif parity_calc='1' then						-- calculate partial parity
				parity <= parity xor s_data_int;
			end if;
		end if;
	end process p_parity;
	
	p_success : process(clk, rst)
	begin
		if rst='1' then
			success <= '0';
		elsif rising_edge(clk) then
			if check_parity = '1' then
				success <= not( not(parity) xor s_data_int);
			elsif check_ack = '1' then
				success <= success and not(s_data_int);
			end if;
		end if;
	end process p_success;
	
	-- =======================================================
	--		máquina de estados del driver (recepción)
	-- =======================================================
	p_current_state : process(clk, rst)
	begin
		if rst='1' then
			current_state <= idle_st;
			read_byte <= (others => '0');
		elsif rising_edge(clk) then
			current_state <= next_state;
			read_byte <= frame;
		end if;
	end process p_current_state;
	
	-- idle_st, rx_read_st, rx_wait_rise_st, rx_wait_fall_st, rx_parity_check, rx_success_st, rx_error_st
	-- calcula el siguiente estado
	p_next_state : process(current_state, count_output, s_clk_int, s_data_int, parity)
	begin
		next_state <= current_state; 
		case current_state is
			------------------------------------------------------------------------------
			-- idle
			------------------------------------------------------------------------------
			when idle_st =>
				if s_clk_int='0' and s_data_int = '0' then						-- si se baja la linea de clk, se va a recibir un dato
					next_state <= rx_wait_rise_st;
				elsif start_write='1' then
					next_state <= tx_clk_down_st;
				end if;
			when wait_release_st =>
				if s_clk_int = '1' and s_data_int = '1' then
					if success = '1' then
						next_state <= success_st;
					else 
						next_state <= error_st;
					end if;
				end if;
			when success_st =>
				next_state <= idle_st;
			when error_st =>
				next_state <= idle_st;
			------------------------------------------------------------------------------
			-- receive states
			------------------------------------------------------------------------------
			when rx_read_st =>
				next_state <= rx_wait_rise_st;
			when rx_par_check_st =>
				next_state <= rx_wait_rise_st;
			when rx_wait_rise_st =>
				if s_clk_int = '1' then
					next_state <= rx_wait_fall_st;
				end if;
			when rx_wait_fall_st =>
				if s_clk_int = '0' then
					if count_output = "1000" then
						next_state <= rx_par_check_st;
					elsif count_output = "1001" then
						next_state <= wait_release_st;
					else
						next_state <= rx_read_st;
					end if;
				end if;
			------------------------------------------------------------------------------
			-- send states
			------------------------------------------------------------------------------
				-- enviar el send_request al dispositivo
			when tx_clk_down_st =>
				if delay_end = '1' then
					next_state <= tx_data_down_st;
				end if;
			when tx_data_down_st =>
				if delay_end = '1' then
					next_state <= tx_release_clk_st;
				end if;
			when tx_release_clk_st =>
				if delay_end = '1' then
					if s_clk_int = '0' then
						next_state <= tx_wait_rise_st;
					end if;
				end if;
				-- enviar datos
			when tx_wait_rise_st =>	
				if s_clk_int = '1' then
					next_state <= tx_wait_fall_st;
				end if;
			when tx_wait_fall_st =>
				if s_clk_int = '0' then
					if count_output = "1000" then
						next_state <= tx_par_wait_rise_st;
					else
						next_state <= tx_next_data_st;
					end if;
				end if;
			when tx_next_data_st =>
				next_state <= tx_wait_rise_st;
				-- enviar paridad
			when tx_par_wait_rise_st =>
				if s_clk_int = '1' then
					next_state <= tx_par_wait_fall_st;
				end if;
			when tx_par_wait_fall_st =>
				if s_clk_int = '0' then
					next_state <= tx_release_data_st;
				end if;
				-- liberar la linea de datos y esperar el ack
			when tx_release_data_st =>
				if s_clk_int = '1' then
					next_state <= tx_wait_ack_st;
				end if;
			when tx_wait_ack_st =>
				if s_clk_int = '0' then
					next_state <= tx_ack_check_st;
				end if;
			when tx_ack_check_st =>
				next_state <= wait_release_st;
		end case;
	end process p_next_state;

	busy <= '0' when current_state = idle_st else '0';
	error <= '1' when current_state = error_st else '0';
	data_ready <= '1' when current_state = success_st else '0';
	count_restart <= '1' when current_state = idle_st else '0';
	delay_restart <= '1' when current_state = idle_st else '0';
	
	-- calcula las salidas en función del estado actual (máquina de moore)
	p_state_machine_outputs : process(current_state, count_output)
	begin
			frame_shift <= '0';
			parity_calc <= '0';
			count_en <= '0';
			check_parity <= '0';
			check_ack <= '0';
		
		case current_state is
			------------------------------------------------------------------------------
			-- idle
			------------------------------------------------------------------------------
			when idle_st =>
				count_en <= '1';
			when wait_release_st =>
			when success_st =>
			when error_st =>
			------------------------------------------------------------------------------
			-- receive outputs
			------------------------------------------------------------------------------
			when rx_read_st =>
				count_en <= '1';
				parity_calc <= '1';
				frame_shift <= '1';
			when rx_par_check_st =>
				count_en <= '1';
				check_parity <= '1';
			when rx_wait_rise_st =>
			when rx_wait_fall_st =>
			------------------------------------------------------------------------------
			-- send outputs
			------------------------------------------------------------------------------
				-- enviar el send_request al dispositivo
			when tx_clk_down_st =>
				s_clk_write <= '1';
				s_clk_value <= '0'; 
				delay_max_count <= "0000000000000000";  -- 100ms
				delay_en <= '1';
				
			when tx_data_down_st =>
				s_clk_write <= '1';
				s_data_write <= '1';
				s_clk_value <= '0';
				s_data_value <= '0';
				delay_max_count <= "0000000000000000";  -- 20ms
				delay_en <= '1';
			when tx_release_clk_st =>
				s_data_write <= '1';
				s_data_value <= '0';
				delay_max_count <= "0000000000000000";  -- enought time for debouncer
				delay_en <= '1';
				-- enviar datos
			when tx_wait_rise_st =>
				s_data_write <= '1';
				s_data_value <= frame(0);
			when tx_wait_fall_st =>
				s_data_write <= '1';
				s_data_value <= frame(0);
			when tx_next_data_st =>
				frame_shift <= '1';
				parity_calc <= '1';
				count_en <= '1';
				-- enviar paridad
			when tx_par_wait_rise_st =>
				s_data_write <= '1';
				s_data_value <= parity xor frame(0);
			when tx_par_wait_fall_st =>
				s_data_write <= '1';
				s_data_value <= parity xor frame(0);
				-- liberar la linea de datos y esperar el ack
			when tx_release_data_st =>
			when tx_wait_ack_st =>
			when tx_ack_check_st =>
				check_ack <= '1';
		end case;
	end process p_state_machine_outputs;

end behavioral;

