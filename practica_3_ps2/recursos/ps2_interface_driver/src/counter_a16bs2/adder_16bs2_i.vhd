----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    15:06:26 10/24/2014 
-- design name: 
-- module name:    adder_16bs2 - behavioral 
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
--
-- dependencies: 
--
-- revision: 
-- revision 0.01 - file created
-- additional comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity adder_16bs2_i is
	port(
		clk			: in std_logic;
		rst			: in std_logic;
		en			: in std_logic;
		restart	: in std_logic;
		op_a		: in std_logic_vector(15 downto 0);
		result		: out std_logic_vector(15 downto 0)
		);
end adder_16bs2_i;

architecture behavioral of adder_16bs2_i is

	-- component declaration
	component adder_8b is
		port(
			op_a			: in std_logic_vector(7 downto 0);
			op_b			: in std_logic_vector(7 downto 0);
			carr_in		: in std_logic;
			result		: out std_logic_vector(7 downto 0);
			carr_out	   : out std_logic
			);
	end component adder_8b;

	component adder_8b_nco is
		port(
			op_a			: in std_logic_vector(7 downto 0);
			op_b			: in std_logic_vector(7 downto 0);
			carr_in		: in std_logic;
			result		: out std_logic_vector(7 downto 0)
			);
	end component adder_8b_nco;
	
	-- signal declaration
	signal carr, carr_ret    : std_logic;
	signal op_a_ret          : std_logic_vector(7 downto 0);
	signal result_ret        : std_logic_vector(7 downto 0);

begin

	add_low : adder_8b
		port map(
			op_a		 => op_a(7 downto 0),
			op_b		 => "00000001",
			carr_in	 => '0',
			result	 => result_ret,
			carr_out	 => carr
			);

	add_high : adder_8b_nco
		port map(
			op_a		 => op_a_ret,
			op_b		 => "00000000",
			carr_in	 => carr_ret,
			result	 => result(15 downto 8)
			);
	

	p_carr_ret : process(clk, rst)
	begin
		if rst='1' then
			carr_ret <= '0';
			op_a_ret <= (others => '0');
			result(7 downto 0) <= (0 => '1', others => '0');
		elsif rising_edge(clk) and en='1' then
				carr_ret <= carr;
			if restart='1' then
				op_a_ret <= (others => '0');
				result(7 downto 0) <= (0 => '1', others => '0');
			else
				op_a_ret <= op_a(15 downto 8);
				result(7 downto 0) <= result_ret;		
			end if;
		end if;
	end process p_carr_ret;
	

end behavioral;

