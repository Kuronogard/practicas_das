----------------------------------------------------------------------------------
-- company: 
-- engineer:  daniel pinto rivero
-- 
-- create date:    10:04:28 10/24/2014 
-- design name: 
-- module name:    tb_adder_8b - testbench 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_ps2_interface_driver is
end tb_ps2_interface_driver;

architecture testbench of tb_ps2_interface_driver is

	-- component declaration
	component ps2_interface_driver is
		port(
			clk			: in std_logic;
			rst			: in std_logic;
			s_clk			: in std_logic;
			s_data		: in std_logic;
			data_ready	: out std_logic;
			--busy			: out std_logic;
			read_byte	: out std_logic_vector(7 downto 0)
		);
	end component ps2_interface_driver;

	
	-- constant declaratinos
	constant clk_period : time := 10 us;
	constant s_clk_period	: time := clk_period*200;
	
	-- signal declarations
	signal clk, rst : std_logic;
	signal s_clk, s_data : std_logic;
	signal data_ready, busy, write_start: std_logic;
	signal received_byte : std_logic_vector(7 downto 0);
	
	
	-- variable reclarations
	signal start_clock : std_logic;
	
begin
	
	i_dut : ps2_interface_driver
		port map(
			clk					=> clk,
			rst					=> rst,
			s_clk				=> s_clk,
			s_data			=> s_data,
			data_ready	=> data_ready,
			--busy				=> busy,
			read_byte		=> received_byte
		);
	
	
	p_rst : process
	begin
		rst <= '1';
		wait for clk_period*2;
		wait until falling_edge(clk);
		rst <= '0';
		wait;
	end process p_rst;
	
	p_clk : process
	begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
	end process p_clk;
	
	p_s_clk : process
	begin
		s_clk <= '1';
		wait until rst='0';
		wait for clk_period*5;
		wait until falling_edge(clk);
		for i in 0 to 10 loop
			wait for s_clk_period/2;
			s_clk <= '0';
			wait for s_clk_period/2;
			s_clk <= '1';
		end loop;
		wait;
	end process p_s_clk;
	
	p_stim : process
		variable data : std_logic_vector(10 downto 0) := "11001110010";   -- 00111001 parity->1
	begin
		s_data <= '1';
		for i in 0 to 10 loop
			s_data <= data(i);
			wait until rising_edge(s_clk);
		end loop;
		wait;
	end process p_stim;

end testbench;

