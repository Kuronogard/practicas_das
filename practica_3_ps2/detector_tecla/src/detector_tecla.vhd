----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    17:45:40 11/20/2014 
-- design name: 
-- module name:    detector_tecla - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity detector_tecla is
	port(
		clk					: in std_logic;
		rst					: in std_logic;
		s_data			: in std_logic;
		s_clk				: in std_logic;
		digit_out		: out std_logic_vector(7 downto 0);
		display_sel	: out std_logic_vector(3 downto 0)
	);
end detector_tecla;

architecture behavioral of detector_tecla is


	-- component declarations
	component bcd_to_d7seg is
		port(
			input		: in std_logic_vector(3 downto 0);
			output	: out std_logic_vector(7 downto 0)
		);
	end component bcd_to_d7seg;

	component freqdiv_50m250h is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			clk_250h	: out std_logic
		);
	end component freqdiv_50m250h;

	component d7segm4_driver is
		port(
			clk						: in std_logic;
			rst						: in std_logic;
			en						: in std_Logic;
			show					: in std_logic;
			display_0			: in std_logic_vector(7 downto 0);
			display_1			: in std_logic_vector(7 downto 0);
			display_2			: in std_logic_vector(7 downto 0);
			display_3			: in std_logic_vector(7 downto 0);
			display_in			: out std_logic_vector(7 downto 0);
			display_sel		: out std_logic_vector(3 downto 0)
		);
	end component d7segm4_driver;
	
	component ps2_interface_driver is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			s_clk				: in std_logic;
			s_data			: in std_logic;
			data_ready	: out std_logic;
			read_byte		: out std_logic_vector(7 downto 0)
		);
	end component ps2_interface_driver;
	
	-- signal declarations
	signal ps2_data_ready, clk_250h : std_logic;
	signal ps2_recv_byte, last_read_byte : std_logic_vector(7 downto 0);
	signal display_0, display_1 : std_logic_vector(7 downto 0);

begin

	p_last_read_key : process(clk, rst)
	begin
		if rst='1' then
			last_read_byte <= (others => '0');
		elsif rising_edge(clk) and ps2_data_ready='1' then
			last_read_byte <= ps2_recv_byte;
		end if;
	end process p_last_read_key;

	i_d7seg_driver_freq_div : freqdiv_50m250h
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> '1',
			clk_250h	=> clk_250h
		);

	i_d7seg_driver : d7segm4_driver
		port map(
			clk						=> clk,
			rst						=> rst,
			en						=> clk_250h,
			show					=> '1',
			display_0			=> display_0,
			display_1			=> display_1,
			display_2			=> "11111111",
			display_3			=> "11111111",
			display_in			=> digit_out,
			display_sel		=> display_sel
		);

	i_7seg_display_0 : bcd_to_d7seg
		port map(
			input		=> last_read_byte(3 downto 0),
			output	=> display_0
		);

	i_7seg_display_1 : bcd_to_d7seg
		port map(
			input		=> last_read_byte(7 downto 4),
			output	=> display_1
		);
		
	i_ps2_driver : ps2_interface_driver
		port map(
			clk					=> clk,
			rst					=> rst,
			s_clk				=> s_clk,
			s_data			=> s_data,
			data_ready	=> ps2_data_ready,
			read_byte		=> ps2_recv_byte
		);
		

end behavioral;

