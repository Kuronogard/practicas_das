----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    18:18:24 10/24/2014 
-- design name: 
-- module name:    counter - behavioral 
-- project name: 
-- target devices: 
--
-- while en='1', counts to "max_count" and then reset to 0 to
-- keep counting
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity timerctc_f is
	generic(
		g_width : natural := 8
	);
	port(
		clk       : in std_logic;
		rst       : in std_logic;
		en        : in std_logic;
		max_count : in std_logic_vector(g_width-1 downto 0);
		reached   : out std_logic
	);
end timerctc_f;

architecture behavioral of timerctc_f is

	-- component declaration
	component timer_f is
		generic(
			g_width : natural := 8
		);
		port(
			clk            : in std_logic;
			rst             : in std_logic;
			en             : in std_logic;
			restart       : in std_logic;
			count			 : out std_logic_vector(g_width-1 downto 0)
		);
	end component timer_f;	
	
	--signal declaration
	signal current_count  : std_logic_vector(g_width-1 downto 0);
	signal timer_restart, reached_prev : std_logic;

begin

	i_timer : timer_f
		generic map(
			g_width => g_width
		)
		port map(
			clk        => clk,
			rst        => rst,
			en        => en,
			restart  => timer_restart,
		   count    => current_count
		);
	
	timer_restart <= '1' when current_count = max_count else '0';
	reached <= '1' when current_count /= max_count and reached_prev = '1' else '0';
	
	p_reached : process(clk, rst)
	begin
		if rst='1' then
			reached_prev <= '0';
		elsif rising_edge(clk) then
			if current_count = max_count then
				reached_prev <= '1';
			else
				reached_prev <= '0';
			end if;
		end if;
	end process p_reached;
	
end behavioral;

