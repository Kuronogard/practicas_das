----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    14:09:44 11/08/2014 
-- design name: 
-- module name:    d7segm4_driver - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity d7segm4_driver is
	port(
		clk						: in std_logic;
		rst						: in std_logic;
		en						: in std_Logic;
		show					: in std_logic;
		display_0			: in std_logic_vector(7 downto 0);
		display_1			: in std_logic_vector(7 downto 0);
		display_2			: in std_logic_vector(7 downto 0);
		display_3			: in std_logic_vector(7 downto 0);
		display_in			: out std_logic_vector(7 downto 0);
		display_sel		: out std_logic_vector(3 downto 0)
	);
end d7segm4_driver;

architecture behavioral of d7segm4_driver is
	
	-- signal declarations
	signal display_sel_int : std_logic_vector(3 downto 0);

begin

	display_sel <= display_sel_int;

	p_display : process(clk, rst)
	begin
		if rst='1' then
			display_sel_int <= "1111";
			display_in <= "11111111";
		elsif rising_edge(clk) and en='1' then
			case display_sel_int is
				when "1111" =>  display_sel_int <= "1110";
				when "1110" => display_in <= display_1;
				when "1101" => display_in <= display_2;
				when "1011" => display_in <= display_3;
				when "0111" => display_in <= display_0;
				when others =>	display_in <= "11111111";
			end case;
			if show = '1' then
				case display_sel_int is
					when "1111" => display_sel_int <= "1110";
					when "1110" => display_sel_int <= "1101";
					when "1101" => display_sel_int <= "1011";
					when "1011" => display_sel_int <= "0111";
					when "0111" => display_sel_int <= "1110";
					when others =>	display_sel_int <= (others =>'1');
				end case;
			else
				display_sel_int <= "1111";
			end if;
		end if;
	end process p_display;

end behavioral;

