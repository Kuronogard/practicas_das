----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    10:49:16 10/30/2014 
-- design name: 
-- module name:    edge_detector - behavioral 
-- project name: 
----------------------------------------------------------------------------------

--	component rise_edge_detector is
--		port(
--			clk				: in std_logic;
--			rst	   		: in std_logic;
--			en				: in std_logic;
--			signal_in	: in std_logic;
--			edge			: out std_logic
--		);
--	end component edge_detector;
--	
--	i_rise_detector : rise_edge_detector
--		port map(
--			clk				=> ,
--			rst	   		=> ,
--			en				=> ,
--			signal_in	=> ,
--			edge			=>
--		);


library ieee;
use ieee.std_logic_1164.all;

entity rise_edge_detector is
	port(
		clk				: in std_logic;
		rst	   		: in std_logic;
		en				: in std_logic;
		signal_in	: in std_logic;
		edge			: out std_logic
	);
end rise_edge_detector;

architecture behavioral of rise_edge_detector is
	signal signal_prev : std_logic;
begin
	p_signal_prev : process(clk, rst)
	begin
		if rst='1' then
			signal_prev <='0';
		elsif rising_edge(clk) then
			if en='1' then
				signal_prev <= signal_in;
			end if;
		end if;
	end process p_signal_prev;
	
	edge <= signal_in and (not signal_prev);
	
end behavioral;

