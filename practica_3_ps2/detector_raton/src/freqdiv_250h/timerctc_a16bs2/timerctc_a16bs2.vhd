----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    18:18:24 10/24/2014 
-- design name: 
-- module name:    counter - behavioral 
-- project name: 
-- target devices: 
--
-- while en='1', counts to "max_count" and then reset to 0 to
-- keep counting
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity timerctc_a16bs2 is
	port(
		clk       : in std_logic;
		rst       : in std_logic;
		en        : in std_logic;
		max_count : in std_logic_vector(15 downto 0);
		reached   : out std_logic
	);
end timerctc_a16bs2;

architecture behavioral of timerctc_a16bs2 is

	-- component declaration
	component adder_16bs2_i is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			op_a			: in std_logic_vector(15 downto 0);
			result		: out std_logic_vector(15 downto 0)
			);
	end component adder_16bs2_i;
	
	--signal declaration
	signal current_count, add_output : std_logic_vector(15 downto 0);

begin
	
	i_add : adder_16bs2_i
		port map(
			clk				=> clk,
			rst				=> rst,
			en				=> en,
			op_a			=> current_count,
			result		=> add_output
			);

	p_current_count : process(clk, rst)
	begin
		if rst='1' then
			current_count <= (others => '0');
		elsif rising_edge(clk) and en='1' then
			if current_count = max_count then
				current_count <= (others => '0');
			elsif current_count = "0000000000000000" then
				current_count <= ( 0 => '1', others=> '0');
			else
				current_count <= add_output;
			end if;
		end if;
	end process p_current_count;
	
	reached <= '1' when current_count = max_count else '0';
	
end behavioral;

