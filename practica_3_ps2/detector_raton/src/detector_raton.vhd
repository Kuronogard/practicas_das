----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    17:45:40 11/20/2014 
-- design name: 
-- module name:    detector_tecla - behavioral 
-- project name: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity detector_raton is
	port(
		clk							: in std_logic;
		rst							: in std_logic;
		s_data					: in std_logic;
		s_clk						: in std_logic;
		ps2_buff_empty	: out std_logic;
		ps2_buff_full		: out std_logic;
		ps2_mouse_of		: out std_logic_vector(1 downto 0);
		digit_out				: out std_logic_vector(7 downto 0);
		display_sel			: out std_logic_vector(3 downto 0)
	);
end detector_raton;

architecture behavioral of detector_raton is

	
	-- component declarations
	component fifo_circular is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			write_en		: in std_logic;
			read_en		: in std_logic;
			write_value	: in std_logic_vector(7 downto 0);
			read_value	: out std_logic_vector(7 downto 0);
			full					: out std_logic;
			empty			: out std_logic
		);
	end component fifo_circular;
	
	component freqdiv_50m250h is
		port(
			clk				: in std_logic;
			rst				: in std_logic;
			en				: in std_logic;
			clk_250h	: out std_logic
		);
	end component freqdiv_50m250h;

	component bcd_to_d7seg is
		port(
			input		: in std_logic_vector(3 downto 0);
			output	: out std_logic_vector(7 downto 0)
		);
	end component bcd_to_d7seg;

	component d7segm4_driver is
		port(
			clk						: in std_logic;
			rst						: in std_logic;
			en						: in std_Logic;
			show					: in std_logic;
			display_0			: in std_logic_vector(7 downto 0);
			display_1			: in std_logic_vector(7 downto 0);
			display_2			: in std_logic_vector(7 downto 0);
			display_3			: in std_logic_vector(7 downto 0);
			display_in			: out std_logic_vector(7 downto 0);
			display_sel		: out std_logic_vector(3 downto 0)
		);
	end component d7segm4_driver;
	
	component ps2_interface_driver is
		port(
			clk					: in std_logic;
			rst					: in std_logic;
			s_clk				: in std_logic;
			s_data			: in std_logic;
			data_ready	: out std_logic;
			read_byte		: out std_logic_vector(7 downto 0)
		);
	end component ps2_interface_driver;
	
	-- type definitions
	type d7seg_type is array(0 to 3) of std_logic_vector(7 downto 0);
	
	-- signal declarations
	signal clk_250h : std_logic;
	signal display : d7seg_type;
	signal fifo_write_en, fifo_read_en : std_logic;
	signal fifo_write, fifo_read : std_logic_vector(7 downto 0);
	signal mouse_coord : std_logic_vector(31 downto 0);	-- lcoordenadas x e y de 16 bits cada una

begin
	---------------------------------------------------------------------------------------------------------
	-- controlador para mostrar los datos en los d7seg
	---------------------------------------------------------------------------------------------------------
	i_clk250h : freqdiv_50m250h
		port map(
			clk					=> clk,
			rst					=> rst,
			en					=> '1',
			clk_250h		=> clk_250h
		);

	bcd_gen : for i in 0 to 3 generate
		i_7seg_display : bcd_to_d7seg
			port map(
				input		=> mouse_coord( (i*8)+7 downto i*8),
				output	=> display(i)
			);
	end generate;

	i_d7seg_driver : d7segm4_driver
		port map(
			clk					=> clk,
			rst					=> rst,
			en					=> clk_250h,
			show				=> '1',
			display_0		=> display(0),
			display_1		=> display(1),
			display_2		=> display(2),
			display_3		=> display(3),
			display_in		=> digit_out,
			display_sel	=> display_sel
		);
		
	---------------------------------------------------------------------------------------------------------
	-- El controlador de ps2 (escribe en la fifo cada vez que haya un dato listo)
	---------------------------------------------------------------------------------------------------------
	i_ps2_driver : ps2_interface_driver
		port map(
			clk					=> clk,
			rst					=> rst,
			s_clk				=> s_clk,
			s_data			=> s_data,
			data_ready	=> fifo_write_en,
			read_byte		=> fifo_write
		);
		
	---------------------------------------------------------------------------------------------------------
	-- la coordenada actual en 32 bits,
	-- los 16 menos significativos son el valor de "x" y el resto el de "y"
	---------------------------------------------------------------------------------------------------------
	p_coord: process(clk, rst)
	begin
		if rst='1' then
			mouse_coord <= (others => '0');
		elsif rising_edge(clk) then
			
		end if;
	end process p_coord;
	

	---------------------------------------------------------------------------------------------------------
	-- la fifo para mediar entre el controlador de ps2 y el resto
	---------------------------------------------------------------------------------------------------------
	i_fifo_ps2 : fifo_circular
		port map(
			clk					=> clk,
			rst					=> rst,
			write_en		=> fifo_write_en,
			read_en		=> fifo_read_en,
			write_value	=> fifo_write,
			read_value	=> fifo_read,
			full					=> ps2_buff_full,
			empty			=> ps2_buff_empty
		);

end behavioral;

