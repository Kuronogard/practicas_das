----------------------------------------------------------------------------------
-- author:		Daniel Pinto Rivero
-- create date:		09:42:44 10/24/2014 
-- module name:	adder_8b - behavioral 
-- project name: 
-- target devices: 	spartan-3
-- description:   8 bit adder
-- 	
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity adder_one is
	generic(
		g_width		: natural := 8
		);
	port(
		op				: in std_logic_vector(g_width-1 downto 0);
		result		   : out std_logic_vector(g_width-1 downto 0)
		);
end adder_one;

architecture behavioral of adder_one is
begin

	
	p_result : process(op)
		variable carry : std_logic_vector(g_width downto 0);
	begin
		carry(0) := '1';
		for i in 0 to g_width-1 loop
			carry(i+1) := ( op(i) and carry(i) );
			result(i) <= op(i) xor carry(i);
		end loop;
	end process p_result;


end behavioral;

