----------------------------------------------------------------------------------
-- company: 
-- engineer: 
-- 
-- create date:    17:01:17 11/23/2014 
-- design name: 
-- module name:    fifo_circular - behavioral 
-- project name: 
-- target devices: 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fifo_circular is
	port(
		clk					: in std_logic;
		rst					: in std_logic;
		write_en		: in std_logic;
		read_en		: in std_logic;
		write_value	: in std_logic_vector(7 downto 0);
		read_value	: out std_logic_vector(7 downto 0);
		full					: out std_logic;
		empty			: out std_logic
	);
end fifo_circular;

architecture behavioral of fifo_circular is

	-- component declarations
	component adder_one is
		generic(
			g_width		: natural := 8
			);
		port(
			op				: in std_logic_vector(g_width-1 downto 0);
			result		   : out std_logic_vector(g_width-1 downto 0)
			);
	end component adder_one;

	-- constant declaratins
	constant c_fifo_length : natural := 16;


	-- type definitions
	type fifo_type is array(0 to c_fifo_length-1) of std_logic_vector(7 downto 0);

	-- signal declarations
	signal fifo : fifo_type;
	
	signal empty_int, full_int : std_logic;
	signal write_dir, read_dir, add_write_result, add_read_result : std_logic_vector(3 downto 0);  -- log(c_fifo_length)-1

begin

	i_fifo_read_dir_adder : adder_one
		generic map(
			g_width		=> 4
			)
		port map(
			op				=> read_dir,
			result		   => add_read_result
			);

	i_fifo_write_dir_adder : adder_one
		generic map(
			g_width		=> 4
			)
		port map(
			op				=> write_dir,
			result		   => add_write_result
			);

	read_value <= fifo(to_integer(unsigned(read_dir)));
	empty <= empty_int;
	full <= full_int;	

	empty_int <= '1' when  add_read_result = write_dir else '0';
	full_int <= '1' when read_dir = write_dir else '0';
	
	p_last_read : process(clk, rst)
	begin
		if rst='1' then
			for i in 0 to c_fifo_length-1 loop
				fifo(i) <= (others => '0');
			end loop;
				read_dir <= (others => '0');
				write_dir <= (0 => '1', others => '0');
		elsif rising_edge(clk) then
			if write_en='1' and full_int='0' then
				fifo(to_integer(unsigned(write_dir))) <= write_value;
				write_dir <= add_write_result;
			end if;
			if read_en='1' and empty_int='0' then
				read_dir <= add_read_result;
			end if;
		end if;
	end process p_last_read;

end behavioral;

